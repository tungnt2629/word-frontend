import TextEditor from "./TextEditor"
import Home from './pages/home'
import Login from './pages/login'
import Register from './pages/register'
import Admin from './pages/admin/index'
import About from './pages/user/about'
import Forgot from './pages/user/forgot'
import Department from './pages/department/index'
import NotFound from './pages/notfound'
import Faq from './pages/faq/index'
import UserManagement from './pages/admin/userManagement'
import TemplateManagement from './pages/admin/templateManagement'
import TemplateManagementDetail from './pages/admin/templateManagementDetail'
import TemplateEditor from './pages/admin/template/templateEditor'
import DocumentManagement from './pages/admin/documentManagement'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom"
// import { v4 as uuidV4 } from "uuid"
import { Provider } from 'react-redux'
import store from './store'
import { createBrowserHistory } from 'history'
import { useHistory } from "react-router"
import { React, useEffect } from 'react'
import ApiService from "./services"

function App() {
  
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/" exact>
            <Home/>
          </Route>
          <Route path="/login" exact>
            <Login/>
          </Route>
          <Route path="/register" exact>
            <Register/>
          </Route>
          <Route path="/admin" exact>
            <Admin/>
          </Route>
          <Route path="/admin/department" exact>
            <Admin/>
          </Route>
          <Route path="/admin/user-management" exact>
            <UserManagement/>
          </Route>
          <Route path="/admin/template-management" exact>
            <TemplateManagement/>
          </Route>
          <Route path="/admin/template-management/:id" exact>
            <TemplateManagementDetail/>
          </Route>
          <Route path="/admin/template-management/edit/:id" exact>
            <TemplateEditor/>
          </Route>
          <Route path="/admin/document-management" exact>
            <DocumentManagement/>
          </Route>
          <Route path="/about" exact>
            <About/>
          </Route>
          <Route path="/forgot" exact>
            <Forgot/>
          </Route>
          <Route path="/faq" exact>
            <Faq/>
          </Route>
          {/* <Route path="/documents">
            <Redirect to={`/documents/}`} />
          </Route> */}
          <Route path="/documents/:id" exact>
            <TextEditor />
          </Route>
          <Route path="/department" exact>
            <Department/>
          </Route>
          <Route path="/department/:id" exact>
            <Department/>
          </Route>
          <Route component={NotFound} />
        </Switch>
      </Router>
    </Provider>
  )
}

export default App
