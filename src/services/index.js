import axios from 'axios'
// import { history } from '../App'
// https://cors-everywhere.herokuapp.com/http://myapi.com/v1/users
// axios.defaults.baseURL = process.env.BASE_API + '/api'
// axios.defaults.baseURL = 'http://localhost:8000/api'
// axios.defaults.baseURL = 'http://ec2-13-250-48-154.ap-southeast-1.compute.amazonaws.com/api'
axios.defaults.baseURL = 'https://wordmanagement.xyz/api'
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'
axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*'
axios.defaults.headers.delete['Content-Type'] = 'application/json'
axios.defaults.headers.put['Content-Type'] = 'application/json'
axios.interceptors.request.use(request => {
  let token = ApiService.getCookie('token')
  
  if (token) {
    request.headers.common.Authorization = `Bearer ${token}`
  } else { 
    console.log("Session timed out!")
    // history.push('/login')
  }
  return request
})

export const ApiService = {
  get(url) {
    return axios.get(`${url}`).catch(error => console.log(error))
  },
  query(url, params) {
    return axios.get(`${url}`, { params }).catch(error => console.log(error))
  },
  post(url, params, config) {
    return axios.post(`${url}`, params, config)
  },
  delete(url, params, config) {
    return axios.delete(`${url}`, params, config)
  },
  put(url, params, config) {
    return axios.put(`${url}`, params, config)
  },
  saveToCookie(name, value) {
    var date = new Date()
    date.setTime(date.getTime()+(39000*1000))
    var expiry = '; expires=' + date.toUTCString()

    document.cookie = `${name} = ${value}`+ expiry
  },
  getCookie(cname) {
    let name = cname + "="
    let decodedCookie = decodeURIComponent(document.cookie)
    let ca = decodedCookie.split(';')
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i]
      while (c.charAt(0) === ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ""
  },
  deleteCookieByName(name) {
    document.cookie = name +
    '=; expires=Thu, 01-Jan-70 00:00:01 GMT;'
  },
  deleteAllCookies() {
    var cookies = document.cookie.split(";")
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i]
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT"
    }
  },
  getQueryVariable(variable)
  {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
    if(pair[0] === variable){return pair[1]}
      }
    return(false);
  }
}

export default ApiService
