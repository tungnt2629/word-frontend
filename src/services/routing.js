import ApiService from "."
import { message, Button, Space } from 'antd';

export const checkTokenAuth = {
    getAuth(history) {
        let token = ApiService.getCookie('token')
        if (!token) {
            history.push('/login')
            window.location.reload()
            message.error("Phiên làm việc đã hết hạn, vui lòng đăng nhập lại !")
        }
    }
}
export default checkTokenAuth