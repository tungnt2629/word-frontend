import React, { useCallback, useEffect, useState } from "react"
import { Button, Row, Col, Space, Image, Menu, Dropdown, Input, Tooltip, message, Divider, Alert, Empty } from 'antd'
import {
  ShareAltOutlined,
  FileWordTwoTone,
  StarOutlined,
  SaveOutlined,
  SyncOutlined,
  InfoCircleOutlined,
  LogoutOutlined,
  CommentOutlined,
  CloseOutlined,
  UnorderedListOutlined,
  CheckCircleTwoTone,
  EyeTwoTone,
  EditTwoTone
} from '@ant-design/icons'
import Quill from "quill"
import "quill/dist/quill.snow.css"
import { io } from "socket.io-client"
import { Link, useParams } from "react-router-dom"
import { useHistory } from "react-router"
import ApiService from "./services"
import { deleteComments, listComments, saveComment, setListCommentAndUserComments, shareDocPublic, shareDocument, showDocument, showSharePerson, updateDocument } from "./actions/document"
import checkTokenAuth from "./services/routing"
import { useSelector, useDispatch } from 'react-redux'
import { viewProfile } from "./actions/auth"
// import QuillCursors, { Cursor } from 'quill-cursors'
import { saveAs } from 'file-saver';
import * as quillToWord from 'quill-to-word'
// import CanvasDraw from "react-canvas-draw";
// import $ from 'jquery';
import Modal from "antd/lib/modal/Modal"
import { listUser } from "./actions/user"
import moment from 'moment'
// require('https').globalAgent.options.rejectUnauthorized = false;
// const { convertTextToDelta } = require('node-quill-converter')
// import QuillAuthorship from 'quill-authorship'

const SAVE_INTERVAL_MS = 7000
const TOOLBAR_OPTIONS = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],

  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered' }, { 'list': 'bullet' }],
  [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction

  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean']                                         // remove formatting button
]

const metaData = []
const { SubMenu } = Menu

export default function TextEditor() {
  const dispatch = useDispatch()
  const history = useHistory()

  const [title, setTitle] = useState('Loading ...')
  const { id: documentId } = useParams()
  const [socket, setSocket] = useState()
  const [quill, setQuill] = useState()
  const [loadingSave, setLoadingSave] = useState(false)
  // const [drawPopUp, setDrawPopup] = useState(false)
  const [shareModel, setShareModel] = useState(false)
  const [listCommentAndUser, setListCommentAndUser] = useState(null)
  const [publicState, setPublicState] = useState(0)
  const [hideComment, setHideComment] = useState(true)
  const [editPerson, setEditPerson] = useState([])
  const [lastTimeEdit, setLastTimeEdit] = useState('')
  // const [drawInstance, setDrawInstance] = useState()

  // const saveableCanvas = useRef(null)
  // const saveUrlImageProject = useRef(null)
  // const saveUrlImageBoard = useRef(null)

  const auth = useSelector(state => state.auth)
  const user = useSelector(state => state.user)
  const docs = useSelector(state => state.document)

  // model share
  const showModalShare = () => {
    setShareModel(false);
  };

  const handleOkShare = () => {
    setShareModel(false);
  };

  const handleCancelShare = () => {
    setShareModel(false);
  };
  // end modal share

  // modal draw
  // const showModal = () => {
  //   setDrawPopup(true);
  // };

  // const handleOk = () => {
  //   setDrawPopup(false);
  // };

  // const handleCancel = () => {
  //   setDrawPopup(false);
  // };

  // end modal draw

  useEffect(() => {
    dispatch(viewProfile(history))
    dispatch(listComments(history, { docId: documentId }))
  }, [])

  useEffect(() => {
    if (auth.profile) {
      dispatch(showSharePerson(history, {
        receiveId: auth.profile.id,
        documentId: documentId
      }))
    }
  }, [auth.profile])

  useEffect(() => {
    if (!ApiService.getCookie('token')) {
      history.push('/login')
    }
  }, [])

  useEffect(() => {
        const s = io("https://wordmanagement.xyz:3001" ,  { transports: 
     ['websocket'], upgrade: false }, { 'force new connection': true })
    // const s = io("http://localhost:3001")
    setSocket(s)

    return () => {
      s.disconnect()
    }
  }, [])

  useEffect(() => {
    if (socket == null || quill == null) return

    checkTokenAuth.getAuth(history)
    ApiService.get('/documents/show/' + documentId)
      .then((response) => {
        if (response.data.status === 200) {
          // response.data.data['usera'] = auth.profile.name
          socket.emit("get-document", response.data.data)
          setLastTimeEdit(response.data.data.updated_at)
          setTitle(response.data.data.title)
          setPublicState(response.data.data.public)
        }
      })

    socket.once("load-document", document => {
      const delta = quill.clipboard.convert(document)

      quill.setContents(delta, 'silent')
      quill.enable()
    })
  }, [socket, quill, documentId])

  useEffect(() => {
    if (socket == null || quill == null) return
    const interval = setInterval(() => {
      setLoadingSave(true)
      let titleTmp = ''
      ApiService.get('/documents/show/' + documentId)
        .then((response) => {
          if (response.data.status === 200) {
            titleTmp = response.data.data.title
            // Call api update document
            const newDocument = {
              title: title,
              content: quill.root.innerHTML
            }
            // console.log('newDocument', newDocument)
            ApiService.post('/documents/update/' + documentId, newDocument)
              .then((response) => {
                setLoadingSave(false)
                if (response.status === 200) {
                  
                } else {
                  history.push('/login')
                }
              })
              .catch((error) => {
              })
          }
        })

    }, SAVE_INTERVAL_MS)

    return () => {
      clearInterval(interval)
      
    }
  }, [socket, quill, title])

  useEffect(() => {
    if (socket == null || quill == null) return

    const handler = delta => {
      quill.updateContents(delta)
    }
    socket.on("receive-changes", handler)

    return () => {
      socket.off("receive-changes", handler)
    }
  }, [socket, quill])

  useEffect(() => {
    if (socket == null) return

    const handler = data => {
      message.info(`Người dùng đang chỉnh sửa tài liệu cùng bạn: ${data.name}`, [8])
      // console.log('===============>', data)
      // setEditPerson(data)
    }
    socket.once("receive-users", handler)

    return () => {
      socket.off("receive-users", handler)
    }
  }, [socket])

  useEffect(() => {
    if (socket == null || quill == null) return
    const handler = (delta, oldDelta, source) => {
      if (source !== "user") return
      socket.emit("send-changes", delta)
      socket.emit("send-users", auth.profile)
    }
    quill.on("text-change", handler)

    return () => {
      quill.off("text-change", handler)
    }
  }, [socket, quill])

  const wrapperRef = useCallback(wrapper => {
    if (wrapper == null) return

    wrapper.innerHTML = ""
    const editor = document.createElement("div")
    wrapper.append(editor)
    // Quill.register('modules/cursors', QuillCursors);
    const q = new Quill(editor, {
      theme: "snow",
      modules: {
        toolbar: TOOLBAR_OPTIONS,
        // cursors: {
        //   template: '<div class="custom-cursor">...</div>',
        //   hideDelayMs: 5000,
        //   hideSpeedMs: 0,
        //   selectionChangeSource: null,
        //   transformOnTextChange: true,
        // },
      }
    })
    q.root.spellcheck = false;
    q.disable()
    q.setText("Loading...")

    setQuill(q)
  }, [])

  
  const handleTitleChange = (e) => {
    setTitle(e.target.value)
  }

  const handlePressEnter = (e) => {
    const newDocument = {
      title: title,
      content: quill.root.innerHTML
    }
    // console.log("--------------->==========>", newDocument)
    ApiService.post('/documents/update/' + documentId, newDocument)
      .then((response) => {
        if (response.status === 200) {
        } else {
          history.push('/login')
        }
      })
      .catch((error) => {
      })
  }

  useEffect(() => {
    if (quill) {
      // const cursors = quill.getModule('cursors');
      //   cursors.createCursor(1, 'Tung', 'blue' )
      quill.on("selection-change", function (range, oldRange, source) {
        // console.log("Local cursor change: ", range); 

        // const cursorsOne = quill.getModule("cursors");
        // get cursor
        // console.log("cursors from A:", cursorsOne)
        // cursorsOne.createCursor('cursor', 'User 2','', 'blue');
        
      });
    }
  }, [quill])

  const exportDocx = async () => {
    if (quill && title) {
      const delta = quill.getContents();
      const quillToWordConfig = {
        exportAs: 'blob'
      };
      const docAsBlob = await quillToWord.generateWord(delta, quillToWordConfig);
      saveAs(docAsBlob, title);
    }
  }
  const menu = (
    <Menu>
      {/* <Menu.Item key={1}>
        <a rel="noopener noreferrer" onClick={() => {
          exportDocx()
        }}>
          Tải xuống (.docx)
        </a>
      </Menu.Item> */}
      <SubMenu title="Tải xuống">
        <Menu.Item key="1" onClick={() => {
          exportDocx()
        }}>Tài liệu .docx</Menu.Item>
      </SubMenu>
    </Menu>
  )
  const viewMenu = (
    <Menu>
      <SubMenu title="Chế độ xem">
        <Menu.Item key="2" onClick={() => {
          if(quill) {
            quill.enable()
          }
        }}><EditTwoTone/> Chỉnh sửa </Menu.Item>
        <Menu.Item key="3" onClick={() => {
          if(quill) {
            quill.disable()
            message.info("Bạn đang xem tài liệu với chế độ chỉ xem!")
          }
        }}><EyeTwoTone/>  Chỉ xem </Menu.Item>
      </SubMenu>
    </Menu>
  )

  const avatarMenu = (
    <Menu>
      <Menu.Item danger onClick={() => {
        ApiService.deleteAllCookies()
        history.push("/login")
      }}>
        <Space>
          <LogoutOutlined />
          Đăng xuất
        </Space>
      </Menu.Item>
      <Menu.Item onClick={() => {
        history.push("/about")
      }}>
        <Space>
          <InfoCircleOutlined />
          Thông tin cá nhân
        </Space>
      </Menu.Item>
    </Menu>
  )
  useEffect(() => {
    async function fetchData() {
      dispatch(listUser(history))
    }
    fetchData()
  }, [])

  // // DRAWING
  // const drawingRef = useCallback(wrapper => {
  //   // console.log(wrapper)
  //   // setDrawInstance(wrapper)
  // }, [])

  // // current.canvasContainer.childNodes[1].toDataURL()

  // // if(drawInstance) console.log('-->', drawInstance)
  useEffect(() => {
    if (docs.listComments.length !== 0 && docs.listUserComments.length !== 0) {
      const arrInfoMap = new Map(docs.listUserComments.map(o => [o.id, o]))
      dispatch(setListCommentAndUserComments(docs.listComments.map(o => ({ ...o, ...arrInfoMap.get(o.user_id) }))))
    }
  }, [docs.listComments, docs.listUserComments])
  
  // console.log('1', editPerson )
  
  return (
    <>
      {
        auth.profile &&
        <React.Fragment>
          <div className="header">
            <Row gutter={40} className="navbar" justify="space-between" align="middle">
              <Col>
                <Space >
                  <FileWordTwoTone className="logo" onClick={
                    () => {
                      history.push('/')
                    }
                  } />

                  <div className="title-wrapper">
                    <Space className="title-space">
                      <Input
                        // value={document.currentDocument.title}
                        value={title}
                        className="document-title"
                        onChange={e => handleTitleChange(e)}
                        onPressEnter={e => {
                          handlePressEnter(e)
                        }}
                      // onPressEnter={(e) => console.log(e.target.value)}
                      ></Input>
                      <StarOutlined />
                      {
                        loadingSave === true ?
                          <>
                            <SyncOutlined spin />Đang lưu
                          </>
                          :
                          <>
                            <SaveOutlined />Đã lưu
                          </>
                      }
                      {/* <CloudUploadOutlined /> */}
                    </Space>
                    <h4 className="document-subtitle">
                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Tài liệu</Button>
                      </Dropdown>
                      <Dropdown overlay={viewMenu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Hiển thị</Button>
                      </Dropdown>
                      
                      <u><CheckCircleTwoTone /> Chỉnh sửa lần cuối vào {(moment.utc()).diff(moment(lastTimeEdit), "minutes") || '...'} phút trước</u>
                    </h4>
                      
                  </div>
                </Space>
              </Col>
              <Col className="avatar-wrapper">
                <Space className="header-center">
                  {
                    editPerson.length > 0 &&
                    <>
                    {
                      editPerson && editPerson.map((editItem, key) => (
                        <Image className='share-person-avatar' src={editItem.avatar}/>
                      ))}
                    </>
                  }
                  {
                    docs.sharePersons.length > 0 &&
                    <>
                      {
                        docs.sharePersons && docs.sharePersons.map((shareItem, key) => (
                          <span key = {key + 1}>
                            <Tooltip color={'blue'} title={
                              <>
                                <Alert message="Người chia sẻ tài liệu" type="success" showIcon className="share-person-break" />
                                <div>Tên: {shareItem.name}</div>
                                <div>SĐT: {shareItem.phone}</div>
                                <div>Giới tính: {shareItem.gender}</div>
                                <div>Ngày sinh: {moment(shareItem.dob).format('DD/MM/YYYY')}</div>
                                <div>Email: {shareItem.email}</div>
                              </>
                            }>
                              <Image className='share-person-avatar' src={shareItem.avatar}/>
                            </Tooltip>
                          </span>
                        ))
                      }
                    </>
                  }
                  <Tooltip title='Bình luận'>
                    <Button type='primary' id="comment-button" onClick={() => {
                      // cần chuyển thành modal nhìn cho đẹp
                      let prompt = window.prompt("Nhập nội dung bình luận với nội dung đã chọn", "");
                      let txt;
                      if (prompt === null || prompt === "") {
                        txt = "Ng dùng out ra";
                      } else {
                        // get curent sellection ?
                        let range = quill.getSelection();
                        if (range) {
                          if (range.length === 0) {
                            alert("Bạn chưa trỏ chuột vào khu vực tài liệu", range.index);
                          } else {
                            let text = quill.getText(range.index, range.length);
                            // get được selection quill --> lưu vào bảng comment
                            let commentData = {
                              user_id: auth.profile.id,
                              document_id: parseInt(documentId),
                              content: prompt,
                              index: range.index,
                              length: range.length
                            }
                            dispatch(saveComment(history, commentData))
                            let rangeAndComments = { range: range, comment: prompt }
                            metaData.push(rangeAndComments);
                            // setselection cần range.index và range.length
                            // set hightlight color 
                            quill.formatText(range.index, range.length, {
                              background: "#fff72b"
                            });
                          }
                        } else {
                          alert("Vui lòng chọn đoạn văn bản bạn muốn bình luận");
                        }
                      }
                    }}><CommentOutlined />Bình luận</Button>
                  </Tooltip>
                  <Button
                    type="primary"
                    icon={<ShareAltOutlined />}
                    onClick={
                      () => {
                        setShareModel(!shareModel)
                      }
                    }
                  >Chia sẻ</Button>
                  <Dropdown overlay={avatarMenu}>
                    <Image
                      alt="avatar"
                      className="avatar"
                      src={`https://ui-avatars.com/api/?name=${auth.profile.name}`}
                    />
                  </Dropdown>
                </Space>
              </Col>
            </Row>
          </div>

          <div className="editor-comment-list-wrapper">
            {/* <UnorderedListOutlined/> */}
            {
              hideComment === false ?
              <div className="editor comment-hide-button editor-comment-list" >
                <UnorderedListOutlined onClick={()=>{
                setHideComment(!hideComment)
              }}/>
              </div>
              :
              <div className='editor-comment-list'>
              <div className='editor-comments'>
                <Tooltip color={'blue'} title='Chọn các bình luận để xem chi tiết'>
                  <Alert message="DANH SÁCH CÁC BÌNH LUẬN" type="info" showIcon 
                    action={
                      <UnorderedListOutlined onClick={()=>{
                        setHideComment(!hideComment)
                      }}/>
                    }
                  />
                </Tooltip>
                <Divider />
                {
                  docs.listComments.length !== 0 && docs.listUserComments.length !== 0 ?
                  <>
                    {docs.listCommentAndUserComments.map((item, key) => (
                      <Row>
                        <Link alt='' onClick={() => {
                          if (quill) {
                            quill.setSelection(parseInt(item.index), parseInt(item.length))
                          }
                        }} className="editor commentor">
                          <Tooltip title={
                            <Row>
                              <Space>
                                <Col><Image src={item.avatar}></Image></Col>
                                <Col>
                                  <div>Tên: {item.name}</div>
                                  <div>Số điện thoại: {item.phone}</div>
                                  <div>Giới tính: {item.gender}</div>
                                  <div>Ngày sinh: {moment(item.dob).format('DD/MM/YYYY')}</div>
                                  <div>Email: {item.email}</div>
                                </Col>
                              </Space>
                            </Row>
                          } placement="left" color={'blue'}>
                            <span className="editor black-text">Người bình luận: </span>{item.name}
                          </Tooltip>
                          {/* Mã người dùng: {item.user_id} */}
                          <div>
                            <span className="editor black-text">Nội dung: </span>{item.content}
                          </div>
                        </Link>
                        <Tooltip title={'Xóa bình luận này'} placement="right" color={'red'} className="delete-comment-icon">
                          <CloseOutlined 
                            onClick={()=> {
                              console.log('Xoas', auth.profile.id, 'item', item.id, item.name, item.email)
                              console.log('Xoas', documentId)
                              if (auth.profile.id === item.user_id) {
                                // console.log('abc-----------------------------------xxxxxxxxxxxxxxx-x-x-x-x-x-', item.user_id)
                                let dataDeleteComment = {
                                  userId: item.user_id,
                                  docId: documentId,
                                  content: item.content
                                }
                                dispatch(deleteComments(history, dataDeleteComment))
                              } else {
                                message.warning('Chỉ người viết bình luận mới được xóa bình luận của mình !', [4])
                              }
                            }}
                            
                          />
                        </Tooltip>
                      </Row>
                    ))}
                  </>
                  :
                  <>
                    <Empty description={"Chưa có bình luận nào !"} className="list-comment-empty"/>
                  </>
                }
              </div>
            </div>
            }
          </div>
          <div className="container" ref={wrapperRef}></div>

          {/* <Modal 
        visible={drawPopUp} 
        title="Thêm hình vẽ" 
        onOk={handleOk} 
        onCancel={handleCancel}
        className="canvas-field"
        footer={[
          <Button type='danger' onClick={handleCancel}
          >
            Thoát
          </Button>,
          <Button onClick={handleOk} type='primary'>Chèn hình ảnh</Button>
        ]}
      > */}

          {/* <CanvasDraw
          // ref={(canvasDraw) => (saveableCanvas.current = canvasDraw)}
          // loadTimeOffset="0"
          // brushColor="red"
          // catenaryColor="#00000000"
          // canvasWidth="100%"
          // canvasHeight="100%"
          // lazyRadius="2"
          // brushRadius="2"
          // hideGrid="true"
          // onChange={formChange}
          // imgSrc={props.url_image ? props.url_image : ''}
          ref={drawingRef}
        />
        <Button onClick={()=> { 
            if (drawInstance.current) {
              // this will get the canvas HTML element where everyhting that's painted is drawn
              // and call the toDataURL() function on it
              console.log(drawInstance.current.canvasContainer.children[1].toDataURL());
            }
          console.log(drawInstance)}} typr='primary'>Save Drawing</Button> */}
          {/* </Modal> */}

          <Modal
            visible={shareModel}
            title="Chia sẻ tài liệu này"
            onOk={handleOkShare}
            onCancel={handleCancelShare}
            footer={[
            ]}
          >
            {
              publicState === 1 ?
                <>
                  <Alert message="Tài liệu này đã được chia sẻ đến tất cả mọi người" type="success" showIcon closable/>
                  <div className='unshare-list-all'
                    onClick={() => {
                      setPublicState(0)
                      dispatch(shareDocPublic({
                        title: title,
                        public: 0
                      }, documentId, history, false))
                    }}
                  >Hủy chia sẻ tài liệu này cho mọi người</div>
                </>
                :
                <>
                  <div className='share-list-all'
                    onClick={() => {
                      setPublicState(1)
                      dispatch(shareDocPublic({
                        title: title,
                        public: 1
                      }, documentId, history, true))
                    }}
                  >Chia sẻ tài liệu này cho mọi người</div>
                </>
            }
            <Divider />
            <Alert message="Danh sách người dùng có thể chia sẻ" type="info" />
            {
              user.listUsers && user.listUsers.map((item, key) => (
                <>
                  {
                    item.id !== auth.profile.id &&
                    <Row key={key + 1} className='share-list' align="middle">
                      <Col>
                        <Image src={item.avatar}/>
                      </Col>
                      <Col><Tooltip title={item.email}>
                        {item.name}
                      </Tooltip></Col>
                      <Col>
                        <Button
                          type="primary"
                          onClick={() => {
                            // console.log('user id to share ', item.id, 'curent doc id', parseInt(documentId), 'curent user id', auth.profile.id)
                            let shareInfo = {
                              from_id: auth.profile.id,
                              receive_id: item.id,
                              status: 'edit',
                              document_id: parseInt(documentId)
                            }
                            let receiverName = item.name
                            dispatch(shareDocument(history, shareInfo, receiverName))
                          }
                          }
                        >
                          <ShareAltOutlined /> Chia sẻ
                        </Button>
                      </Col>
                    </Row>
                  }
                </>
              ))
            }
          </Modal>
        </React.Fragment>
      }
    </>
  )
}
