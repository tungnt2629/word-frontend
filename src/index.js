import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import "./styles/styles.css"
import "./styles/admin.css"
import "./styles/editor.css"
import "./styles/globals.css"
import "./styles/user.css"
import 'antd/dist/antd.css'

ReactDOM.render(
  // <React.StrictMode>
    <App />,
  // </React.StrictMode>,
  document.getElementById("root")
)
