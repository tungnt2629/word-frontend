import { message } from 'antd'
import ApiService from '../services'
import checkTokenAuth from '../services/routing'
import { AUTH, SET_LOADING } from './types'
import { listUser } from './user'

export const login = (data, history) => {
  return (dispatch) => {
    dispatch(setLoading(true))
    ApiService.post('/login', data)
      .then((response) => {
        if (response.data.status === 200) {
          dispatch(setErrorMessage(null))
          ApiService.saveToCookie('token', response.data.access_token)
          dispatch(setLoading(false))
          history.push('/')
        } else if (response.data.status === 401) {
          dispatch(setLoading(false))
          dispatch(setErrorMessage(response.data.message))
        } else {
          dispatch(setLoading(false))
          dispatch(setErrorMessage(response.data.message.email[0]))
        }
      })
      .catch((error) => {
        dispatch(setLoading(false)) 
      })
  }
}

export const viewProfile = (history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/me')
      .then((response) => {
        if (response.data.status === 200) {
          dispatch(setCurrentUser(response.data.data))
        }
        else {
          history.push('/login')
        }
      })
      .catch((error) => {
      })
  )
}

export const addUser = (data, history) => {
  return (dispatch) => {
    dispatch(setLoading(true))
    ApiService.post('/register', data)
      .then((response) => {
        if (response.data.status === 422) {
          dispatch(setLoading(false))
          dispatch(setErrorMessage(response.data.message.email))
          message.error(response.data.message.email)
        } else if (response.data.status === 200) {
          dispatch(setLoading(false))
          dispatch(viewProfile(history))
          dispatch(setErrorMessage(null))
          message.success('Thêm mới người dùng thành công!')
          // if (admin === '') {
          //   history.push('/login')
          // } else {
            dispatch(setLoading(false))
            
            dispatch(listUser(history))
          // }
        } else {
          dispatch(setLoading(false))
          message.error('Vui lòng kiểm tra lại thông tin đang ký tài khoản!')
        }
      })
      .catch((error) => {
        dispatch(setLoading(false))
      })
  }
}
export const register = (data, history, admin='') => {
  return (dispatch) => {
    dispatch(setLoading(true))
    ApiService.post('/register', data)
      .then((response) => {
        if (response.data.status === 422) {
          dispatch(setLoading(false))
          dispatch(setErrorMessage(response.data.message.email))
          message.error(response.data.message.email)
        } else if (response.data.status === 200) {
          dispatch(setLoading(false))
          dispatch(viewProfile(history))
          dispatch(setErrorMessage(null))
          
          if (admin === '') {
            history.push('/login')
          } else {
            dispatch(setLoading(false))
            
            dispatch(listUser(history))
          }
        } else {
          dispatch(setLoading(false))
          message.error('Vui lòng kiểm tra lại thông tin đang ký tài khoản!')
        }
      })
      .catch((error) => {
        dispatch(setLoading(false))
      })
  }
}

export const setCurrentUser = (payload) => {
  return {
    type: AUTH.SET_USER,
    payload
  }
}

export const setErrorMessage = (payload) => {
  return {
    type: AUTH.SET_ERROR_MESSAGE,
    payload
  }
}

export const setLoading = (payload) => {
  return {
    type: SET_LOADING,
    payload
  }
}