import ApiService from '../services'
import { USERS } from './types'
import checkTokenAuth from '../services/routing'
import { message } from 'antd'

export const createUser = (data, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/users/create', data
    )
      .then((response) => {
        if (response.data.status === 200) {
          message.success("Thêm mới người dùng thành công!")
          dispatch(listUser(history))
        }
      })
      .catch((error) => {
      })
  )
}

export const listUser = (history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/users/listUserAdmin')
      .then((response) => {
        if (response.data.status === 200) {

          console.log('response USERSSS: ', response.data.data.users)
          dispatch(setListUser(response.data.data.users))
        } else {
          message.error('Lấy danh sách người dùng thất bại!')
        }
      })
      .catch((error) => {
      })
  )
}

export const showUser = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/users/show/' + id)
      .then((response) => {
        if (response.data.status === 200) {
          dispatch(setUpdateUser(response.data.data.users))
        //   dispatch(setListTemplate(response.data.data.templates))
        }
        // else message.error('error')
      })
      .catch((error) => {
      })
  )
}

export const updateUser = (data, id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/users/update/' + id, data)
      .then((response) => {
        if (response.status === 200) {
          message.success('Cập nhật thông tin người dùng thành công')
          dispatch(setUpdateUser(response.data.data))
          dispatch(listUser(history))
        } else {
          message.error('Cập nhật người dùng thất bại!')
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const deleteUser = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/users/delete/' + id)
      .then((response) => {
        if (response.data.status && response.data.status === 200) {
          message.success("Xóa người dùng thành công")
          dispatch(listUser(history))
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const setNewUser = (payload) => {
  return {
    type: USERS.CREATE_USER,
    payload
  }
}

export const setUpdateUser = (payload) => {
  return {
    type: USERS.UPDATE_USER,
    payload
  }
}

export const setListUser = (payload) => {
  return {
    type: USERS.LIST_USER,
    payload
  }
}