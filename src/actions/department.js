import ApiService from '../services'
import { DEPARTMENTS } from './types'
import checkTokenAuth from '../services/routing'
import { message } from 'antd'

export const createDepartment = (data, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/departments/create', data
    )
      .then((response) => {
        if (response.data.status === 200) {
          message.success("Thêm mới phòng ban thành công!")
          dispatch(listDepartment(history))
        }
      })
      .catch((error) => {
      })
  )
}

export const listDepartment = (history, admin=null) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/departments')
      .then((response) => {
        if (response.data.status === 200) {
          // console.log("list department response: ", response)
          dispatch(setListDepartment(response.data.data.departments))
          if(admin) {
            dispatch(setListTemplate(response.data.data.departments.templates))
          }
        } else {
          message.error('Lấy danh sách phòng ban thất bại!')
        }
      })
      .catch((error) => {
      })
  )
}

export const listDepartmentFromRegisterPage = (history, admin=null) => {
  // checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/departments')
      .then((response) => {
        if (response.data.status === 200) {
          // console.log("list department response: ", response)
          dispatch(setListDepartment(response.data.data.departments))
          if(admin) {
            dispatch(setListTemplate(response.data.data.departments.templates))
          }
        } else {
          message.error('Lấy danh sách phòng ban thất bại!')
        }
      })
      .catch((error) => {
      })
  )
}

export const showDepartment = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/departments/show/' + id)
      .then((response) => {
        if (response.data.status === 200) {
          dispatch(setUpdateDepartment(response.data.data.name))
          dispatch(setListTemplate(response.data.data.templates))
        }
        // else message.error('error')
      })
      .catch((error) => {
      })
  )
}

export const updateDepartment = (data, id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/departments/update/' + id, data)
      .then((response) => {
        if (response.status === 200) {
          if(response.data.status === 200) {
            console.log(response)
            message.success("Cập nhật thông tin phòng ban thành công!")
            dispatch(listDepartment(history))
          }
        } else {
          message.error('Xảy ra lỗi khi cập nhật phòng ban')
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const deleteDepartment = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/departments/delete/' + id)
      .then((response) => {
        // ìf(response.data.status === 200){
        //   message.success("Cập nhật phòng ban thành công!")
        // }
        // console.log("delete department response ===> ", response)
        if (response.data.status && response.data.status === 200) {
          message.success("Xóa phòng ban thành công")
          dispatch(listDepartment(history))
        }
        // if (response.status === 200) {
        //   // dispatch(setUpdateDocument(response.data))
        // } else {
        //   // message.error('Cập nhật phòng ban thất bại!')
        // }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}
export const deleteTemplate = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/templates/delete/' + id)
      .then((response) => {
        if (response.data.status && response.data.status === 200) {
          message.success("Xóa tài liệu mẫu thành công")
          dispatch(showDepartment(id, history))
          dispatch(listDepartment(history))
        }
        else {
          message.error('Chưa xóa được tài liệu mẫu!')
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const setNewDepartment = (payload) => {
  return {
    type: DEPARTMENTS.CREATE_DEPARTMENT,
    payload
  }
}

export const setUpdateDepartment = (payload) => {
  return {
    type: DEPARTMENTS.UPDATE_DEPARTMENT,
    payload
  }
}

export const setListDepartment = (payload) => {
  // console.log("payload DATA", payload)
  return {
    type: DEPARTMENTS.LIST_DEPARTMENT,
    payload
  }
}

export const setListTemplate = (payload) => {
  // console.log("payload DATA", payload)
  return {
    type: DEPARTMENTS.LIST_TEMPLATE,
    payload
  }
}

// export const setLoading = (payload) => {
//   // console.log("payload DATA", payload)
//   return {
//     type: DEPARTMENTS.SET_LOADING,
//     payload
//   }
// }