import ApiService from '../services'
import { DEPARTMENTS } from './types'
import checkTokenAuth from '../services/routing'
import { message } from 'antd'

// import { saveAs } from 'file-saver';
// import * as quillToWord from 'quill-to-word';
// import * as quill from 'quilljs';

export const createTemplate = (id, history) => {
  checkTokenAuth.getAuth(history)
  let data = {
    department_id: id,
    title: 'Chưa có tiêu đề',
    content: ''
  }
  return (dispatch) => (
    ApiService.post('/templates/create', data)
      .then((response) => {
        console.log(response)
        if (response.data.status === 200) {
        //   dispatch(listDepartment(history))
          history.push(`/admin/template-management/edit/${response.data.data.id}?department-id=${id}`)
        } else {
          message.error('Lỗi thêm mới tài liệu mẫu')
        }
      })
      .catch((error) => {
      })
  )
}

export const listDepartment = (history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/departments')
      .then((response) => {
        if (response.data.status === 200) {
          // console.log('list department response: ', response)
          dispatch(setListDepartment(response.data.data.departments))
        } else {
          message.error('Lấy danh sách phòng ban thất bại!')
        }
      })
      .catch((error) => {
      })
  )
}

export const showDepartment = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/departments/show/' + id)
      .then((response) => {
        if (response.data.status === 200) {
          dispatch(setUpdateDepartment(response.data.data.name))
          dispatch(setListTemplate(response.data.data.templates))
        }
        // else message.error('error')
      })
      .catch((error) => {
      })
  )
}

export const updateDepartment = (data, id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/departments/update/' + id, data)
      .then((response) => {
        if (response.status === 200) {
          if(response.data.status === 200) {
            console.log(response)
            message.success('Cập nhật thông tin phòng ban thành công!')
            dispatch(listDepartment(history))
          }
        } else {
          message.error('Xảy ra lỗi khi cập nhật phòng ban')
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const deleteDepartment = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/departments/delete/' + id)
      .then((response) => {
        // ìf(response.data.status === 200){
        //   message.success('Cập nhật tài liệu thành công!')
        // }
        // console.log('delete department response ===> ', response)
        if (response.data.status && response.data.status === 200) {
          message.success('Xóa phòng ban thành công')
          dispatch(listDepartment(history))
        }
        // if (response.status === 200) {
        //   // dispatch(setUpdateDocument(response.data))
        // } else {
        //   // message.error('Cập nhật tài liệu thất bại!')
        // }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const setNewDepartment = (payload) => {
  return {
    type: DEPARTMENTS.CREATE_DEPARTMENT,
    payload
  }
}

export const setUpdateDepartment = (payload) => {
  return {
    type: DEPARTMENTS.UPDATE_DEPARTMENT,
    payload
  }
}

export const setListDepartment = (payload) => {
  // console.log('payload DATA', payload)
  return {
    type: DEPARTMENTS.LIST_DEPARTMENT,
    payload
  }
}

export const setListTemplate = (payload) => {
  // console.log('payload DATA', payload)
  return {
    type: DEPARTMENTS.LIST_TEMPLATE,
    payload
  }
}