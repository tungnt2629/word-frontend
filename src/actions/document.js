import ApiService from '../services'
import { DOCS } from './types'
import checkTokenAuth from '../services/routing'
import { message } from 'antd'

export const createDocument = (data, history) => {
  checkTokenAuth.getAuth(history)
  const newData = {
    title: data.title,
    content: data.content,
    department_id: data.department_id
  }
  return (dispatch) => (
    ApiService.post('/documents/create', newData)
      .then((response) => {
        if (response.data.status === 200) {
          let responseDocument = {
            title: response.data.data.title,
            content: response.data.data.content
          }
          dispatch(setNewDocument(responseDocument))

          history.push('/documents/' + response.data.data.id)
        }
        else {
          // message.error(response.data.message.title[0])
        }
      })
      .catch((error) => {
      })
  )
}

export const listDocumentAdmin = (history, keyword = '') => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/documents?keyword='+keyword)
      .then((response) => {
        if (response.data.status === 200) {
          // console.log("RESPONSEES: ", response)
          dispatch(setListDocument(response.data.data.documents))
        } else {
          message.error('Lấy danh sách tài liệu thất bại!')
        }
      })
      .catch((error) => {
      })
  )
}

export const listDocument = (history, keyword = '', userId) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/documents?keyword='+keyword, userId)
      .then((response) => {
        if (response.data.status === 200) {
          // console.log("RESPONSEES: ", response)
          dispatch(setListDocument(response.data.data.documents))
        } else {
          message.error('Lấy danh sách tài liệu thất bại!')
        }
      })
      .catch((error) => {
      })
  )
}

export const listShare = (history, receiveId) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/share/list', { receiveId: receiveId })
      .then((response) => {
        if (response.data.status === 200) {
          // console.log('00', response)
          // console.log('11', response.data.data.shares)
          // console.log('22', response.data.data.publicDocs)
          // let listShareSpecific = response.data.data.shares.concat(response.data.data.publicDocs)
          // console.log('33', listShareSpecific)
          dispatch(setListDocumentShare(response.data.data.shares))
          dispatch(setPublicDocuments(response.data.data.publicDocs))
        } else {
          console.log('Lấy danh sách tài liệu được share thất bại!')
        }
      })
      .catch((error) => {
      })
  )
}

export const shareDocument = (history, data, receiverName) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/share', data)
      .then((response) => {
        if (response.data && response.data.code === 200) {
          message.success('Đã chia sẻ tài liệu này cho người dùng: '+ receiverName)
        }
        else {
          message.error('Vui lòng thử lại')
        }
      })
      .catch((error) => {
      })
  )
}

export const showSharePerson = (history, data) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/share/show', data)
      .then((response) => {
        if (response.data && response.data.status === 200) {
          dispatch(setSharePersons(response.data.data))
        }
        else {
          message.error('Vui lòng thử lại')
        }
      })
      .catch((error) => {
      })
  )
}

export const showDocument = (id, history) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.get('/documents/show/' + id)
      .then((response) => {
        if (response.data.status === 200) {
          dispatch(setUpdateDocument(response.data.data))
        }
        // else message.error('error')
      })
      .catch((error) => {
      })
  )
}

export const shareDocPublic = (data, id, history , unshare) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/documents/update/' + id, data)
      .then((response) => {
        if (response.data.status === 200) {
          if (unshare === true) { 
            message.success('Bạn đã chia sẻ tài liệu này cho tất cả mọi người !')
          }
          else if (unshare === false) {
            message.success('Bạn đã ngừng chia sẻ tài liệu này cho tất cả mọi người !')
          }
        }
      })
      .catch((error) => {
      })
  )
}

export const updateDocument = (data, id, history) => {
  // console.log('=====> ', data, id)
  checkTokenAuth.getAuth(history)
  const newDocument = {
    title: "Tài liệu chưa có tiêu đề",
    content: data
  }
  return (dispatch) => (
    ApiService.post('/documents/update/' + id, newDocument)
      .then((response) => {
        // console.log("RESPONSE ===> ", response)
        if (response.status === 200) {
          dispatch(setUpdateDocument(response.data))
        } else {
          // message.error('Cập nhật tài liệu thất bại!')
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}
export const deleteDocumentAdmin = (id, history) => {
  // console.log("id")
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/documents/admin/delete/' + id)
      .then((response) => {
        if (response.data.status === 200) {
          message.success("Xóa tài liệu thành công")
          dispatch(setListDocument(response.data.data.documents))
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}
export const deleteDocument = (id, history, data) => {
  // console.log("id")
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/documents/delete/' + id, data)
      .then((response) => {
        if (response.data.status === 200) {
          message.success("Xóa tài liệu thành công")
          dispatch(setListDocument(response.data.data.documents))
          // dispatch(listDocument(history))
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const saveComment = (history, data) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/comments', data)
      .then((response) => {
        if(response.data.code === 200) {
          message.success("Đã thêm bình luận của bạn")
          dispatch(listComments(history, {docId: data.document_id}))
          dispatch(setListComment(response.data.data.listComments))
        }
        else {
          console.log("Lỗi thêm bình luận")
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const listComments = (history, data) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/comments/list', data)
      .then((response) => {
        if(response.data.status === 200) {
          dispatch(setListComment(response.data.data.listComments.list))
          dispatch(setListUserComment(response.data.data.listComments.userDetails))
          let listComments = response.data.data.listComments.list;
          let listUserComments = response.data.data.listUserComments.list;

          // dispatch(setListCommentAndUserComments(response.data.data.listComments.userDetails))
          if (listComments.length !== 0 && listUserComments.length !== 0) {
            const arrInfoMap = new Map(listUserComments.map(o => [o.id, o]))
            let tmp = listComments.map(o => ({ ...o, ...(arrInfoMap).get(o.user_id) }))
          }
        }
        else {
          console.log("Lỗi list doc")
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const deleteComments = (history, data) => {
  checkTokenAuth.getAuth(history)
  return (dispatch) => (
    ApiService.post('/comments/delete', data)
      .then((response) => {
        if(response.data.status === 200) {
          message.success('Xóa bình luân thành công')
          dispatch(setListComment(response.data.data.listComments.list))
          dispatch(setListUserComment(response.data.data.listComments.userDetails))
        }
        else {
          console.log("Lỗi xóa bình luận")
        }
      })
      .catch((error) => {
        console.log(error)
      })
  )
}

export const setNewDocument = (payload) => {
  return {
    type: DOCS.CREATE_DOCUMENT,
    payload
  }
}

export const setUpdateDocument = (payload) => {
  return {
    type: DOCS.UPDATE_DOCUMENT,
    payload
  }
}

export const setListDocument = (payload) => {
  return {
    type: DOCS.LIST_DOCUMENT,
    payload
  }
}

export const setListDocumentShare = (payload) => {
  // console.log(payload)
  return {
    type: DOCS.LIST_DOCUMENT_SHARE,
    payload
  }
}

export const setSharePersons = (payload) => {
  return {
    type: DOCS.SET_SHARE_PERSON,
    payload
  }
}

export const setListComment = (payload) => {
  return {
    type: DOCS.SET_LIST_COMMENT,
    payload
  }
}

export const setListUserComment = (payload) => {
  return {
    type: DOCS.SET_LIST_USER_COMMENT,
    payload
  }
}

export const setListCommentAndUserComments = (payload) => {
  return {
    type: DOCS.SET_LIST_COMMENT_AND_USER_COMMENT,
    payload
  }
}
export const setPublicDocuments = (payload) => {
  return {
    type: DOCS.SET_PUBLIC_DOCUMENT,
    payload
  }
}