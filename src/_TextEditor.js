import React, { useCallback, useEffect, useState } from "react"
import { Button, Row, Col, Space, Image, Menu, Dropdown, Input } from 'antd'
import {
  ShareAltOutlined,
  FileWordTwoTone,
  StarOutlined,
  MessageOutlined,
  SaveOutlined,
  CloudUploadOutlined,
  SyncOutlined,
  InfoCircleOutlined,
  LogoutOutlined
} from '@ant-design/icons'
import Quill from "quill"
import "quill/dist/quill.snow.css"
import { io } from "socket.io-client"
import { useParams } from "react-router-dom"
import { useHistory } from "react-router"
import ApiService from "./services"
import { showDocument } from "./actions/document"
import checkTokenAuth from "./services/routing"
import { useSelector, useDispatch } from 'react-redux'
import { viewProfile } from "./actions/auth"
import QuillCursors from 'quill-cursors';


const SAVE_INTERVAL_MS = 2000
const TOOLBAR_OPTIONS = [
  [{ header: [1, 2, 3, 4, 5, 6, false] }],
  [{ font: [] }],
  [{ list: "ordered" }, { list: "bullet" }],
  ["bold", "italic", "underline"],
  [{ color: [] }, { background: [] }],
  [{ script: "sub" }, { script: "super" }],
  [{ align: [] }],
  ["image", "blockquote", "code-block"],
  ["clean"],
]

export default function TextEditor() {
  const dispatch = useDispatch()
  const history = useHistory()

  const [title, setTitle] = useState('Loading...')
  const { id: documentId } = useParams()
  const [socket, setSocket] = useState()
  const [quill, setQuill] = useState()
  const [savingState, setSavingState] = useState(false)

  const auth = useSelector(state => state.auth)
  
  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  useEffect(() => {
    if (!ApiService.getCookie('token')) {
      history.push('/login')
    }
  }, [])

  useEffect(() => {
    const s = io("http://localhost:3001")
    setSocket(s)

    return () => {
      s.disconnect()
    }
  }, [])

  useEffect(() => {
    if (socket == null || quill == null) return

    socket.once("load-document", document => {
      quill.setContents(document)
      quill.enable()
    })

    checkTokenAuth.getAuth(history)
    ApiService.get('/documents/show/' + documentId)
      .then((response) => {
        if (response.data.status === 200) {
          // dispatch(setUpdateDocument(response.data.data))
          // console.log("RESPONSE BRUH ", response)
          socket.emit("get-document", response.data.data)
          setTitle(response.data.data.title)
        }
        // else message.error('error')
      })
    // socket.emit("get-document", documentId)
  }, [socket, quill, documentId])

  useEffect(() => {
    if (socket == null || quill == null) return

    const interval = setInterval(() => {
      socket.emit("save-document", quill.getContents())
    }, SAVE_INTERVAL_MS)

    return () => {
      clearInterval(interval)
    }
  }, [socket, quill])

  useEffect(() => {
    if (socket == null || quill == null) return

    const interval = setInterval(() => {
      setSavingState(false)
      const newDocument = {
        title: title,
        content: quill.getContents()
      }
      // console.log("title to update, ", newDocument)
      ApiService.post('/documents/update/' + documentId, newDocument)
        .then((response) => {
          // console.log("RESPONSE ===> ", response)
          if (response.status === 200) {
            // dispatch(setUpdateDocument(response.data))
          } else {
            history.push('/login')
            // message.error('Cập nhật tài liệu thất bại!')
          }
        })
        .catch((error) => {
          // history.push('/login')
        })
      // socket.emit("save-document-laravel", quill.getContents(), ApiService.getCookie('token'))
    }, 15000)

    return () => {
      clearInterval(interval)
    }
  }, [socket, quill, title])

  useEffect(() => {
    if (socket == null || quill == null) return

    const handler = delta => {
      quill.updateContents(delta)
    }
    socket.on("receive-changes", handler)

    return () => {
      socket.off("receive-changes", handler)
    }
  }, [socket, quill])

  useEffect(() => {
    if (socket == null || quill == null) return
    const handler = (delta, oldDelta, source) => {
      setSavingState(true)
      if (source !== "user") return
      socket.emit("send-changes", delta)
    }
    quill.on("text-change", handler)

    return () => {
      quill.off("text-change", handler)
    }
  }, [socket, quill])

  const wrapperRef = useCallback(wrapper => {
    if (wrapper == null) return

    wrapper.innerHTML = ""
    const editor = document.createElement("div")
    wrapper.append(editor)
    Quill.register('modules/cursors', QuillCursors);

    const q = new Quill(editor, {
      theme: "snow",
      modules: {
        toolbar: TOOLBAR_OPTIONS,
        cursors: {
          template: '<div className="custom-cursor">...</div>',
          hideDelayMs: 5000,
          hideSpeedMs: 0,
          selectionChangeSource: null,
          transformOnTextChange: true,
        }
      },
    })
    q.disable()
    q.setText("Loading...")
    setQuill(q)
  }, [])

  const handleTitleChange = (e) => {
    setSavingState(true)
    setTitle(e.target.value)
  }

  // console.log("title: ", title)
  const menu = (
    <Menu>
      <Menu.Item key={1}>
        <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
          1st menu item
        </a>
      </Menu.Item>
      <Menu.Item key={2}>
        <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
          2nd menu item
        </a>
      </Menu.Item>
      <Menu.Item key={3}>
        <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
          3rd menu item
        </a>
      </Menu.Item>
    </Menu>
  )

  const avatarMenu = (
    <Menu>
      <Menu.Item danger onClick={() => {
        ApiService.deleteAllCookies()
        history.push("/login")
      }}>
        <Space>
          <LogoutOutlined />
          Đăng xuất
        </Space>
      </Menu.Item>
      <Menu.Item onClick={() => {
        history.push("/about")
      }}>
        <Space>
          <InfoCircleOutlined />
          Thông tin cá nhân
        </Space>
      </Menu.Item>
    </Menu>
  )
  return (
    <>
    {
      auth.profile &&
      <React.Fragment>
      <div className="header">
        <Row gutter={40} className="navbar">
          <Col span={6} xs={24} md={15} lg={19} xl={21}>
            <Space >
              <FileWordTwoTone className="logo"/>

              <div className="title-wrapper">
                <Space className="title-space">
                  <Input
                    // value={document.currentDocument.title}
                    value={title}
                    className="document-title"
                    onChange={e => handleTitleChange(e)}
                  // onPressEnter={(e) => console.log(e.target.value)}
                  ></Input>
                  <StarOutlined />
                  {
                    savingState === true ?
                      <SyncOutlined spin />
                      : <SaveOutlined />
                  }
                  <CloudUploadOutlined />
                </Space>
                <h4 className="document-subtitle">
                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">File</Button>
                  </Dropdown>

                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Edit</Button>
                  </Dropdown>

                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">View</Button>
                  </Dropdown>

                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Insert</Button>
                  </Dropdown>

                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Format</Button>
                  </Dropdown>

                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Tool</Button>
                  </Dropdown>

                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Add on</Button>
                  </Dropdown>

                  <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Help</Button>
                  </Dropdown>
                </h4>
              </div>
            </Space>
          </Col>
          <Col span={6} xs={24} md={9} lg={5} xl={3} className="avatar-wrapper">
            {/* <Space> */}
            <Dropdown overlay={menu} placement="bottomRight">
              <MessageOutlined />
            </Dropdown>

            <Dropdown overlay={menu} placement="bottomRight">
              <Button type="" icon={<ShareAltOutlined />}>Chia sẻ</Button>
            </Dropdown>
            <Dropdown overlay={avatarMenu}>
              <Image
                alt="avatar"
                className="avatar"
                src={`https://ui-avatars.com/api/?name=${auth.profile.name}`}
              />
            </Dropdown>
            {/* </Space> */}
          </Col>
        </Row>
      </div>
      <div className="container" ref={wrapperRef}></div>

      <div class="container">
      <div class="editor">
        <h2>User 1</h2>
        <center>(Expanding editor)</center>
        <br/>
        <div id="editor-one"></div>
      </div>

      <div class="editor">
        <h2>User 2</h2>
        <center>(Fixed-height editor)</center>
        <br/>
        <div id="editor-two"></div>
      </div>
    </div>
    </React.Fragment>
    }
    </>
  )
}
