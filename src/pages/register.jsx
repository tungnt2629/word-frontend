import React, { useState, useEffect } from 'react'
import { Button, Col, Form, Input, Row, Alert, Select, DatePicker } from 'antd'
import { UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons'
import { register, setErrorMessage } from '../actions/auth'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from "react-router-dom"
import moment from 'moment'
import { listDepartment, listDepartmentFromRegisterPage } from '../actions/department'

function Register() {
  const history = useHistory()
  const { Option } = Select
  const dispatch = useDispatch()
  const auth = useSelector(state => state.auth)
  const department = useSelector(state => state.department)

  useEffect(() => {
    async function fetchData() {
      dispatch(listDepartmentFromRegisterPage(history))
    }
    fetchData()
  }, [])

  const onSubmit = (values) => {
    delete values.confirm
    values['avatar'] = `https://ui-avatars.com/api/?name=${values.name}`
    values['dob'] = (moment(values.dob).format('DD/MM/YYYY h:mm'))
    dispatch(register(values, history))
  }
  function disabledDate(current) {
    // Can not select days before today and today
    return current && current > moment("2004-01-01", 'YYYY-MM-DD').endOf('day')
  }

  // console.log("auth state from store: ", auth)
  return (
    <div className='register'>
      <Row className="login reg" justify="center" align="middle">
        <Col>
          <Col className="wrapper register-background">
            <Row className="title" justify='center'>
              <Col>
                <UserOutlined className='logo'/>
              </Col>
              <Col>
                Đăng ký tài khoản
              </Col>
            </Row>
            <div className="form">
              {
                auth.error &&
                <Alert
                  message={auth.error}
                  type="error"
                  showIcon
                />
              }
              <Form
                name="basic"
                onFinish={onSubmit}
                autoComplete="off"
              >
                <span>Họ tên</span>
                <Form.Item
                  name="name"
                  rules={[
                    {
                      required: true,
                      message: 'Họ tên không được để trống'
                    }
                  ]}
                >
                  <Input addonAfter={<UserOutlined />} />
                </Form.Item>

                <span>Giới tính</span>
                <Form.Item
                  name="gender"
                  rules={[
                    {
                      required: true,
                      message: 'Giới tính không được để trống'
                    }
                  ]}
                >
                  <Select defaultValue="" style={{ width: '100%' }}>
                    <Option value="nam">Nam</Option>
                    <Option value="nu">Nữ</Option>
                  </Select>
                </Form.Item>

                <span>Thuộc phòng ban</span>
                  <Form.Item
                    name="department_id"
                    rules={[
                      {
                        required: true,
                        message: 'Bạn cần lựa chọn phòng ban'
                      }
                    ]}
                  >
                    <Select defaultValue="" style={{ width: '100%' }}>
                      {
                        department.listDepartments.length !== 0 &&
                        <>
                          {
                            department.listDepartments.map((item, key) => (
                              <Option key={key+1} value={item.id}>{item.name}</Option>
                            ))
                          }
                        </>
                      }
                    </Select>
                  </Form.Item>

                <span>Ngày sinh</span>
                <Form.Item
                  name="dob"
                  rules={[
                    {
                      required: true,
                      message: 'Ngày sinh không được để trống và người đăng kí cần >22 tuổi'
                    }
                  ]}
                >
                  <DatePicker
                    disabledDate={disabledDate}
                    className="date-picker"
                  />
                </Form.Item>

                <span>Số điện thoại</span>
                <Form.Item
                  name="phone"
                  rules={[
                    {
                      required: true,
                      message: 'Số điện thoại không được để trống'
                    }
                  ]}
                >
                  <Input addonAfter={<UserOutlined />} type='number' />
                </Form.Item>

                <span>Email</span>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: 'Email không được để trống'
                    },
                    {
                      type: 'email',
                      message: 'Email không đúng định dạng'
                    }
                  ]}
                >
                  <Input addonAfter={<MailOutlined />} />
                </Form.Item>

                <span>Mật khẩu</span>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: 'Mật khẩu không được để trống'
                    },
                    {
                      min: 8,
                      message: 'Mật khẩu tối thiểu 8 ký tự'
                    },
                    {
                      max: 32,
                      message: 'Mật khẩu tối đa 32 ký tự'
                    }
                  ]}
                >
                  <Input.Password type="password" addonAfter={<LockOutlined />}/>
                </Form.Item>

                <span>Nhập lại mật khẩu</span>
                <Form.Item
                  name="confirm"
                  dependencies={['password']}
                  rules={[
                    {
                      required: true,
                      message: 'Mật khẩu không được để trống'
                    },
                    {
                      min: 8,
                      message: 'Mật khẩu tối thiểu 8 ký tự'
                    },
                    {
                      max: 32,
                      message: 'Mật khẩu tối đa 32 ký tự'
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('password') === value) {
                          return Promise.resolve()
                        }
                        return Promise.reject(new Error('Mật khẩu nhập lại không khớp!'))
                      }
                    })
                  ]}
                >
                  <Input.Password type="password" addonAfter={<LockOutlined />}/>
                </Form.Item>

                {/* <div className='forgot' onClick={()=> router.push('/user/forgot')}><a>Quên mật khẩu</a></div> */}
                <Form.Item>
                  <Button
                    loading={auth.loading}
                    type="primary"
                    htmlType="submit"
                    className="submit"
                    style={{ marginTop:20 }}
                  >
                    Đăng ký
                  </Button>
                </Form.Item>
                <div className='reg'>
                  <span>Bạn đã có tài khoản ? </span>
                  <a onClick={() => {
                    dispatch(setErrorMessage(null))
                    history.push('/login')
                  }}>Đăng nhập</a>
                </div>
              </Form>
            </div>
          </Col>
        </Col>
        
      </Row>
    </div>
  )
}

export default Register
