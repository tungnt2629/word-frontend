import React, { useState, useEffect } from 'react'
import {
  BackTop,
  Row,
  Col,
  Image,
  Dropdown,
  Menu,
  Button,
  Card,
  Avatar,
  Tooltip,
  Input,
  Space,
  Empty,
  message,
  Divider,
  Alert
} from 'antd'
import {
  FileTextTwoTone,
  QuestionOutlined,
  ArrowUpOutlined,
  UnorderedListOutlined,
  EditOutlined,
  InfoCircleOutlined,
  LogoutOutlined,
  DeleteOutlined,
  GoldTwoTone,
  ShareAltOutlined,
  EditTwoTone
} from '@ant-design/icons'
import Meta from 'antd/lib/card/Meta'
import { useSelector, useDispatch } from 'react-redux'
import { viewProfile } from '../actions/auth'
import { deleteDocument, listDocument, listShare, setListComment, setListCommentAndUserComments, setListUserComment, setSharePersons, showDocument, showSharePerson } from '../actions/document'
import { listDepartment, showDepartment } from '../actions/department'
// import CheckAuth from '../middleware/auth'
import { useHistory } from "react-router-dom"
import ApiService from "../services/index"
import logo from '../images/logo.png'
import moment from 'moment'
import Modal from 'antd/lib/modal/Modal'

function Home() {
  const history = useHistory()
  const dispatch = useDispatch()

  const [visible, setVisible] = useState(false)
  const auth = useSelector(state => state.auth)
  const document = useSelector(state => state.document)
  const department = useSelector(state => state.department)

  const [listView, setListView] = useState(false)
  const [listViewShare, setListViewShare] = useState(false)
  const [listViewPublic, setListViewPublic] = useState(false)
  const [deleteModal, setDeleteModal] = useState(false)

  useEffect(() => {
    dispatch(setListComment([]))
    dispatch(setListUserComment([]))
    dispatch(setListCommentAndUserComments([]))
    dispatch(setSharePersons([]))
    dispatch(viewProfile(history))
    dispatch(listDepartment(history))
  }, [])

  useEffect(() => {
    if (auth.profile) {
      dispatch(listShare(history, auth.profile.id))
      // dispatch(showSharePerson(history, auth.profile.id))
      dispatch(listDocument(history, '', { userId: auth.profile.id }))
    }
  }, [auth])

  const handleDeleteDocument = (id, history) => {
    dispatch(deleteDocument(id, history, { 'userId': auth.profile.id }))
  }

  // useEffect(() => {
  //   if(document.listDocuments.length !== 0 && auth){
  //     var array = document.listDocuments,
  //     result = array
  //       .filter(v => v.user_id === auth.profile.id)
  //       .map(v => v);
  //     document.listDocuments = result
  //   }
  // }, [auth, document.listDocuments])

  const { Search } = Input

  const onSearch = value => {
    // console.log(value)
    if (!value || value === '') {
      message.warning("Bạn vui lòng nhập tên tài liệu cần tìm kiếm")
      dispatch(listDocument(history, value, { 'userId': auth.profile.id }))
    } else {
      dispatch(listDocument(history, value, { 'userId': auth.profile.id }))
    }
  }

  const documents = (
    <>
      {
        document.listDocuments.map((item, key) => (
          <React.Fragment key={key + 1}>
            <Col span={6} className="item">

              <Modal
                title="Xóa tài liệu"
                visible={deleteModal}
                onOk={() => setDeleteModal(false)}
                onCancel={() => setDeleteModal(false)}
                footer={[
                  <Button key="back" type='danger' onClick={() => setDeleteModal(false)}>
                    Thoát
                  </Button>,
                  <Button key="submit" type="primary" onClick={() => {
                    handleDeleteDocument(item.id, history)
                    setDeleteModal(false)
                  }}>
                    Xóa
                  </Button>,
                ]}
                centered
              >
                <Alert
                  message={`Bạn có chắc muốn xóa tài liệu "${item.title}" ?`}
                  type="warning"
                  showIcon
                />
              </Modal>

              <Card
                cover={
                  <Image
                    alt="example"
                    src={logo}
                  />
                }
                actions={[
                  <Tooltip key="edit" placement="top" title={'Chỉnh sửa'}>
                    <EditOutlined
                      onClick={() => {
                        dispatch(showDocument(item.id, history))
                        history.push(`/documents/${item.id}`)
                      }}
                    />
                  </Tooltip>,
                  <Tooltip color='red' key="ellipsis" placement="top" title={'Xóa'}>
                    <DeleteOutlined className="delete-icon" onClick={() => {
                      // console.log(" load 1 lan thoi", item.id)
                      setDeleteModal(true)
                    }}
                    />
                  </Tooltip>
                ]}
                className='document-card-global'
              >
                <Meta
                  avatar={<Avatar src={auth.profile.avatar} />}
                  title={
                    <Tooltip color={'blue'} placement="top" title={item.title + ' - ' + item.department.name}>
                      <div>
                        <b>{item.department.name}</b>
                      </div>
                      <div>
                        {item.title}
                      </div>
                    </Tooltip>
                  }
                  //convert  html to text
                  description={<>
                    {
                      item.content !== null &&
                      <>
                        {item.content.replace(/<[^>]+>/g, '').slice(0, 50)}
                      </>
                    }
                  </>}
                  onClick={() => {
                    dispatch(showDocument(item.id, history))
                    history.push(`/documents/${item.id}`)
                  }}
                />
              </Card>
            </Col>
          </React.Fragment>
        ))
      }
    </>
  )
  console.log('1111', document.listDocumentsShare)
  // console.log('2222', document)
  const documentsShare = (
    <>
      {
        document.listDocumentsShare.map((item, key) => (
          <React.Fragment key={key + 1}>
            <Col span={6} className="item">
              <Card
                cover={
                  <Image
                    alt="example"
                    src={logo}
                  />
                }
                actions={[
                  <Tooltip key="edit" placement="top" title={'Chỉnh sửa'}>
                    <EditTwoTone
                      onClick={() => {
                        dispatch(showDocument(item.id, history))
                        history.push(`/documents/${item.id}`)
                      }}
                    />
                  </Tooltip>
                ]}
                className='document-card-global'
              >
                <Meta
                  avatar={<Tooltip color={'blue'} title={
                    <>
                      <div>Tên: {item.user.name}</div>
                      <div>SĐT: {item.user.phone}</div>
                      <div>Giới tính: {item.user.gender}</div>
                      <div>Ngày sinh: {item.user.dob}</div>
                      <div>Email: {item.user.email}</div>
                      <div>{
                        item.user.role === 1 ?
                          <Alert message="Quản trị viên" type="warning" showIcon>Quản trị viên</Alert> :
                          <Alert message="Nhân viên" type="info" showIcon>Quản trị viên</Alert>
                      }</div>
                    </>
                  }>
                    <Avatar src={item.user.avatar} className='share-person-card' />
                  </Tooltip>}
                  title={
                    <Tooltip placement="top" title={item.title + ' - Mã phòng ban: ' + item.department_id}>
                      <div>{item.department.name}</div><div>{item.title}</div>
                    </Tooltip>
                  }
                  //convert  html to text
                  description={
                    <>
                      {
                        item.content !== null &&
                        <>
                          {item.content.replace(/<[^>]+>/g, '').slice(0, 50) + ' ...'}
                        </>
                      }
                    </>
                  }
                  onClick={() => {
                    dispatch(showDocument(item.id, history))
                    history.push(`/documents/${item.id}`)
                  }}
                />
              </Card>
            </Col>
          </React.Fragment>
        ))
      }
    </>
  )
  console.log('dsdsdsds', document.listDocumentsShare.length)
  const documentsPublic = (
    <>
      {
        document.listDocumentsPublic.length !== 0 &&
        <>
          {
            document.listDocumentsPublic.map((item, key) => (
              <React.Fragment key={key + 1}>
                {
                  item.public === 1 &&
                  <>
                    <Col span={6} className="item">
                      <Card
                        cover={
                          <Image
                            alt="example"
                            src={logo}
                          />
                        }
                        actions={[
                          <Tooltip key="edit" placement="top" title={'Chỉnh sửa'}>
                            <EditTwoTone
                              onClick={() => {
                                dispatch(showDocument(item.id, history))
                                history.push(`/documents/${item.id}`)
                              }}
                            />
                          </Tooltip>
                        ]}
                        className='document-card-global'
                      >
                        <Meta
                          avatar={<Tooltip color={'blue'} title={
                            <>
                              <div>Tên: {item.user.name}</div>
                              <div>SĐT: {item.user.phone}</div>
                              <div>Giới tính: {item.user.gender}</div>
                              <div>Ngày sinh: {item.user.dob}</div>
                              <div>Email: {item.user.email}</div>
                              <div>{
                                item.user.role === 1 ?
                                  <Alert message="Quản trị viên" type="warning" showIcon>Quản trị viên</Alert> :
                                  <Alert message="Nhân viên" type="info" showIcon>Quản trị viên</Alert>
                              }</div>
                            </>
                          }>
                            <Avatar src={item.user.avatar} className='share-person-card' />
                          </Tooltip>}
                          title={
                            <Tooltip placement="top" title={item.title}>
                              <div>{item.department.name}</div><div>{item.title}</div>
                            </Tooltip>
                          }
                          //convert  html to text
                          description={
                            <>
                              {
                                item.content !== null &&
                                <>
                                  {item.content.replace(/<[^>]+>/g, '').slice(0, 50) + ' ...'}
                                </>
                              }
                            </>
                          }
                          onClick={() => {
                            dispatch(showDocument(item.id, history))
                            history.push(`/documents/${item.id}`)
                          }}
                        />
                      </Card>
                    </Col>
                  </>
                }
              </React.Fragment>
            ))
          }
        </>
      }
    </>
  )


  return (
    <React.Fragment>
      {
        auth.profile &&
        <div className="page home">
          <div className="header-wrapper">
            <Row className='header'>
              <Col className='col-left' span={4}>
                {/* <Tooltip placement="top" title={'Cài đặt'}>
                  <SettingFilled style={{ fontSize:20 }}/>
                </Tooltip> */}
                <Image
                  style={{ paddingLeft: 15, paddingRight: 15, width: 80 }}
                  className='header-icon'
                  onClick={
                    () => {
                      // dispatch(setListComment([]))
                      // dispatch(setListUserComment([]))
                      history.push('/')
                    }
                  }
                  src={logo}
                />
                <h3>Lisod VN</h3>
                {/* <h3>{auth.profile.avatar}</h3> */}
              </Col>
              <Col span={3}></Col>
              <Col className='col-center' span={10}>
                <Search placeholder="Tìm kiếm tài liệu cá nhân" onSearch={onSearch} enterButton className="search-bar" />
              </Col>
              <Col span={3}></Col>
              <Col className='col-right' span={4} >
                <Tooltip color='blue' placement="top" title={'Thông tin và trợ giúp'}>
                  <QuestionOutlined
                    style={{ paddingLeft: 15, paddingRight: 15 }}
                    onClick={() => {
                      history.push('/faq')
                    }}
                  />
                </Tooltip>
                <Dropdown overlay={
                  <Menu>
                    <Menu.Item key='1' danger onClick={() => {
                      ApiService.deleteAllCookies()
                      history.push("/login")
                    }}>
                      <Space>
                        <LogoutOutlined />
                        Đăng xuất
                      </Space>
                    </Menu.Item>
                    <Menu.Item key='2' onClick={() => {
                      history.push("/about")
                    }}>
                      <Space>
                        <InfoCircleOutlined />
                        Thông tin cá nhân
                      </Space>
                    </Menu.Item>
                    {
                      auth.profile && auth.profile.role === 1 &&
                      <Menu.Item key='3' onClick={() => {
                        history.push("/admin")
                      }}>
                        <Space>
                          <InfoCircleOutlined />
                          Trang quản trị
                        </Space>
                      </Menu.Item>
                    }
                  </Menu>
                }>
                  {
                    auth.profile.avatar ?
                      <Image
                        alt="avatar"
                        className="avatar"
                        src={auth.profile.avatar}
                      />
                      :
                      <Image
                        alt="avatar"
                        className="avatar"
                        src={'https://ui-avatars.com/api/?background=random'}
                      />
                  }
                </Dropdown>
              </Col>
            </Row>
          </div>

          <div className="template-wrapper">
            <div className="content">
              <Row justify="space-between" className="title-wrapper">
                <Col span={12} className="new-document">
                  Tạo tài liệu mới
                </Col>
                <Col span={12} className="template-list">
                </Col>
              </Row>

              <Row className="template-detail" justify="space-between" gutter={[0, 10]}>
                {
                  department.listDepartments && department.listDepartments.map((item, key) => (
                    <React.Fragment key={key + 1}>
                      <Col className="gutter-row department-list" onClick={() => {
                          history.push('/department/' + item.id)
                        }}>
                        <div className="department-name">{item.name}</div>
                        <GoldTwoTone className="new-document" />
                      </Col>
                    </React.Fragment>
                  ))
                }
              </Row>
            </div>
          </div>

          <Row justify="center">
            <Row justify='space-between' className="content-title">
              <Col><b>TÀI LIỆU CÁ NHÂN</b></Col>
              <Col>
                <Tooltip title='Chuyển sang xem dạng danh sách' color={'blue'}>
                  <UnorderedListOutlined
                    className="right-icon"
                    onClick={() => {
                      setListView(!listView)
                    }}
                  />
                </Tooltip>
                {/* <SortAscendingOutlined className="right-icon" />
                <FolderTwoTone
                  className="right-icon"
                  style={{ paddingRight: 0 }}
                /> */}
              </Col>
            </Row>
          </Row>
          {
            listView === false ?
              <>
                {
                  document.listDocuments.length !== 0 ?
                    <div className="content-wrapper">
                      <Row className="main-document">
                        {documents}
                      </Row>
                    </div>
                    :
                    <div className="content-wrapper-empty">
                      <Row className="main-document-empty">
                        <Empty description={"Bạn chưa có tài liệu nào !"} />
                      </Row>
                    </div>
                }
              </>
              :
              <>
                {
                  document.listDocuments.length !== 0 ?
                    <div className="content-wrapper-list">
                      {
                        document.listDocuments.map((item, key) => (
                          <Row className="main-document-list" justify='space-between' type="flex" align="middle">
                            <Col span={7}>
                              <div key={key + 1}>
                                <Tooltip color={'blue'} placement="top" title={item.title + ' - ' + item.department.name}>
                                  <div>
                                    <b>{item.department.name}</b>
                                  </div>
                                  <div>
                                    {item.title}
                                  </div>
                                </Tooltip>
                              </div>
                            </Col>
                            <Col span={14}>
                              <div onClick={() => {
                                dispatch(showDocument(item.id, history))
                                history.push(`/documents/${item.id}`)
                              }}
                              >
                                {
                                  item.content !== null &&
                                  <>
                                    {item.content.replace(/<[^>]+>/g, '').slice(0, 50) + ' ...'}
                                  </>
                                }
                              </div>
                            </Col>
                            <Col>
                              <Space>
                                <Tooltip key="edit" placement="top" title={'Chỉnh sửa'}>
                                  <Button type='primary' onClick={() => {
                                    dispatch(showDocument(item.id, history))
                                    history.push(`/documents/${item.id}`)
                                  }}>
                                    <EditOutlined
                                    />
                                  </Button>
                                </Tooltip>
                                <Tooltip key="ellipsis" placement="top" title={'Xóa'}>
                                  <Button type='danger' onClick={() => {
                                    handleDeleteDocument(item.id, history)
                                  }} >
                                    <DeleteOutlined className="delete-icon list-delete-icon"
                                    />
                                  </Button>
                                </Tooltip>
                              </Space>
                            </Col>
                          </Row>
                        ))
                      }
                    </div>
                    :
                    <div className="content-wrapper-list-empty">
                      <Row className="main-document-list-empty">
                        <Empty description={"Bạn chưa có tài liệu nào !"} />
                      </Row>
                    </div>
                }
              </>
          }
          <Row justify="center" className='share-content-header'>
            <Row justify='space-between' className="content-title">
              <Divider />
              <Col><b>TÀI LIỆU ĐƯỢC CHIA SẺ ĐẾN BẠN <ShareAltOutlined /></b></Col>
              <Col>
                <Tooltip title='Chuyển sang xem dạng danh sách' color={'blue'}>
                  <UnorderedListOutlined
                    className="right-icon"
                    onClick={() => {
                      setListViewShare(!listViewShare)
                    }}
                  />
                </Tooltip>
              </Col>
            </Row>
          </Row>

          {
            listViewShare === false ?
              <>
                {
                  document.listDocumentsShare.length !== 0 ?
                    <div className="content-wrapper">
                      <Row className="main-document">
                        {documentsShare}
                      </Row>
                    </div>
                    :
                    <div className="content-wrapper-empty">
                      <Row className="main-document-empty">
                        <Empty description={"Chưa có ai chia sẻ tài liệu với bạn !"} />
                      </Row>
                    </div>
                }
              </>
              : // list view mục 2
              <>
                {
                  document.listDocumentsShare.length !== 0 ?
                    <div className="content-wrapper-list">
                      {
                        document.listDocumentsShare.map((item, key) => (
                              <>
                                <Row className="main-document-list" justify='space-between' type="flex" align="middle">
                                  <Col span={1}>
                                    <Tooltip color={'blue'} title={
                                      <>
                                        <div>Tên: {item.user.name}</div>
                                        <div>SĐT: {item.user.phone}</div>
                                        <div>Giới tính: {item.user.gender}</div>
                                        <div>Ngày sinh: {item.user.dob}</div>
                                        <div>Email: {item.user.email}</div>
                                        <div>{
                                          item.user.role === 1 ?
                                            <Alert message="Quản trị viên" type="warning" showIcon></Alert> :
                                            <Alert message="Nhân viên" type="info" showIcon></Alert>
                                        }</div>
                                      </>
                                    }>
                                      <Image src={item.user.avatar} className='share-person-avatar' />
                                    </Tooltip>
                                  </Col>
                                  <Col span={7}>
                                    <div key={key + 1}>
                                      <Tooltip placement="top" title={item.title}>
                                        <div><b>{item.department.name}</b></div>
                                        <div><b>{item.title}</b></div>
                                      </Tooltip>
                                    </div>
                                  </Col>
                                  <Col span={12}>
                                    <div onClick={() => {
                                      dispatch(showDocument(item.id, history))
                                      history.push(`/documents/${item.id}`)
                                    }}
                                    >
                                      {
                                        item.content !== null &&
                                        <>
                                          {item.content.replace(/<[^>]+>/g, '').slice(0, 50) + ' ...'}
                                        </>
                                      }
                                    </div>
                                  </Col>
                                  <Col>
                                    <Space>
                                      <Tooltip key="edit" placement="top" title={'Chỉnh sửa'}>
                                        <Button type='primary'>
                                          <EditOutlined
                                            onClick={() => {
                                              dispatch(showDocument(item.id, history))
                                              history.push(`/documents/${item.id}`)
                                            }}
                                          />
                                        </Button>
                                      </Tooltip>
                                    </Space>
                                  </Col>
                                </Row>
                          </>
                        ))
                      }
                    </div>
                    :
                    <div className="content-wrapper-list-empty">
                      <Row className="main-document-list-empty">
                        <Empty description={"Chưa có ai chia sẻ tài liệu với bạn !"} />
                      </Row>
                    </div>
                }
              </>
          }

          {/* ----------------------------- Tài liệu public  --------------------*/}

          <Row justify="center" className='share-content-header'>
            <Row justify='space-between' className="content-title">
              <Divider />
              <Col><b>TÀI LIỆU CHUNG - ĐƯỢC CHIA SẺ ĐẾN TẤT CẢ MỌI NGƯỜI <ShareAltOutlined /></b></Col>
              <Col>
                <Tooltip title='Chuyển sang xem dạng danh sách' color={'blue'}>
                  <UnorderedListOutlined
                    className="right-icon"
                    onClick={() => {
                      setListViewPublic(!listViewPublic)
                    }}
                  />
                </Tooltip>
              </Col>
            </Row>
          </Row>

          {
            listViewPublic === false ?
              <>
                {
                  document.listDocumentsPublic.length !== 0 ?
                    <div className="content-wrapper">
                      <Row className="main-document">
                        {documentsPublic}
                      </Row>
                    </div>
                    :
                    <div className="content-wrapper-empty">
                      <Row className="main-document-empty">
                        <Empty description={"Chưa có ai chia sẻ tài liệu với bạn !"} />
                      </Row>
                    </div>
                }
              </>
              : // list view mục 3
              <>
                {
                  document.listDocumentsPublic.length !== 0 ?
                    <div className="content-wrapper-list">
                      {
                        document.listDocumentsPublic.map((item, key) => (
                          <>
                            {
                              item.public === 1 &&
                              <>
                                <Row className="main-document-list" justify='space-between' type="flex" align="middle">
                                  <Col span={1}>
                                    <Tooltip color={'blue'} title={
                                      <>
                                        <div>Tên: {item.user.name}</div>
                                        <div>SĐT: {item.user.phone}</div>
                                        <div>Giới tính: {item.user.gender}</div>
                                        <div>Ngày sinh: {item.user.dob}</div>
                                        <div>Email: {item.user.email}</div>
                                        <div>{
                                          item.user.role === 1 ?
                                            <Alert message="Quản trị viên" type="warning" showIcon>Quản trị viên</Alert> :
                                            <Alert message="Nhân viên" type="info" showIcon>Quản trị viên</Alert>
                                        }</div>
                                      </>
                                    }>
                                      <Image src={item.user.avatar} className='share-person-avatar' />
                                    </Tooltip>
                                  </Col>
                                  <Col span={7}>
                                    <div key={key + 1}>
                                      <Tooltip placement="top" title={item.title}>
                                        <div><b>{item.department.name}</b></div>
                                        <div><b>{item.title}</b></div>
                                      </Tooltip>
                                    </div>
                                  </Col>
                                  <Col span={12}>
                                    <div onClick={() => {
                                      dispatch(showDocument(item.id, history))
                                      history.push(`/documents/${item.id}`)
                                    }}
                                    >
                                      {
                                        item.content !== null &&
                                        <>
                                          {item.content.replace(/<[^>]+>/g, '').slice(0, 50) + ' ...'}
                                        </>
                                      }
                                    </div>
                                  </Col>
                                  <Col>
                                    <Space>
                                      <Tooltip key="edit" placement="top" title={'Chỉnh sửa'}>
                                        <Button type='primary'>
                                          <EditOutlined
                                            onClick={() => {
                                              dispatch(showDocument(item.id, history))
                                              history.push(`/documents/${item.id}`)
                                            }}
                                          />
                                        </Button>
                                      </Tooltip>
                                    </Space>
                                  </Col>
                                </Row>
                              </>
                            }
                          </>
                        ))
                      }
                    </div>
                    :
                    <div className="content-wrapper-list-empty">
                      <Row className="main-document-list-empty">
                        <Empty description={"Chưa có ai chia sẻ tài liệu với bạn !"} />
                      </Row>
                    </div>
                }
              </>
          }
          <BackTop>
            <div><ArrowUpOutlined /></div>
          </BackTop>
        </div>
      }
    </React.Fragment>
  )
}

// export default CheckAuth(Home)
export default Home