import React, { useEffect, useState, Component } from 'react'
import {
  Row,
  Col,
  Image,
  Menu,
  Button,
  Table,
  Space,
  Modal,
  message,
  Input,
  Form,
  Layout,
  Icon,
  Tooltip,
  Alert
} from 'antd'
import {
  ApartmentOutlined,
  ContainerOutlined,
  DesktopOutlined,
  FileProtectOutlined,
  FileTextOutlined,
  FileWordOutlined,
  LeftOutlined,
  QuestionCircleTwoTone,
  RightOutlined,
  SafetyOutlined,
  TeamOutlined,
  UserOutlined,
  WindowsFilled
} from '@ant-design/icons'

import { useSelector, useDispatch } from 'react-redux'
import { createDepartment, deleteDepartment, deleteTemplate, listDepartment, updateDepartment } from '../../actions/department'
import { useHistory } from "react-router"
import moment from 'moment'
import { viewProfile } from '../../actions/auth'
import logo from '../../images/logo.png'
import { useParams } from "react-router-dom"
import { createTemplate, showDepartment } from '../../actions/template'


function Index() {
  const { id: departmentId } = useParams()
  const history = useHistory()
  const [collapsed, setCollapsed] = useState(false)
  const [sidebarWidth, setSidebarWidth] = useState(3)
  const [mainWidth, setMainWidth] = useState(20)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isModalVisible2, setIsModalVisible2] = useState(false)
  // const [viewState, setViewState] = useState()
  const [titleEditModal, setTitleEditModal] = useState()
  const [recordId, setRecordId] = useState(null)
  const [defaultSelectMenuState, setDefaultSelectMenuState] = useState('3')
  const [deleteModal, setDeleteModal] = useState(false)

  const department = useSelector(state => state.department)
  const auth = useSelector(state => state.auth)

  const dispatch = useDispatch()
  const toggleCollapsed = () => {
    setCollapsed(!collapsed)

    if (!collapsed) {
      setSidebarWidth(1)
      setMainWidth(22)
    } else {
      setSidebarWidth(3)
      setMainWidth(20)
    }
  }

  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  useEffect(() => {
    if(auth && auth.profile) {
      if(auth.profile.role !== 1) {
        message.error("Bạn không phải quản trị viên !")
        history.push("/")
      }
    }
  }, [auth])
  
  const showModal = (name, id) => {
    setTitleEditModal(name)
    setRecordId(id)
    setIsModalVisible(true)
  }

  const handleOk = () => {
    setIsModalVisible(false)
  }

  const handleCancel = () => {
    setIsModalVisible(false)
  }
  // list ra danh sách phòng ban
  const showModal2 = () => {
    setIsModalVisible2(true)
  }

  const handleOk2 = () => {
    setIsModalVisible2(false)
  }

  const handleCancel2 = () => {
    setIsModalVisible2(false)
  }

  useEffect(() => {
    async function fetchData() {
      if(departmentId) {
        dispatch(showDepartment(departmentId, history))
      }
    }
    fetchData()
  }, [])

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      width: '5%',
      render: (text, record) => (
        <>{record.id}</>
      )
    },
    {
      title: 'Tên tài liệu mẫu',
      dataIndex: 'title',
      key: 'title',
      render: (text, record) => (
        <b>{record.title}</b>
      )
    },
    {
      title: 'Nội dung tài liệu mẫu',
      dataIndex: 'content',
      key: 'content',
      width: '35%',
      render: (text, record) => (
        <p className='admin template-content'>
          {
            record.content !== null &&
            <>
              {record.content.replace(/<[^>]+>/g, '').replaceAll('&nbsp','')}
            </>
          }
        </p>
      )
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text, record) => (
        <>{moment(record.created_at).format('DD/MM/YYYY h:mm')}</>
      )
    },
    {
      title: 'Lần cuối chỉnh sửa',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: (text, record) => (
        <>{moment(record.updated_at).format('DD/MM/YYYY h:mm')}</>
      )
    },
    {
      title: 'Chức năng',
      dataIndex: 'functional',
      key: 'functional',
      width: '10%',
      render: (text, record) => (
        <Space>
          <Button type={'primary'} onClick={() => {
            // showModal(record.name, record.id)
            history.push('/admin/template-management/edit/'+record.id+'?department-id='+departmentId)
          }}>
            {'Sửa'}
          </Button>

          <Button type={'danger'} onClick={() => {
            // console.log(record.id)
            setDeleteModal(true)
          }}>
            {"Xóa"}
          </Button>
          <Modal
            title="Xóa tài liệu mẫu"
            visible={deleteModal}
            onOk={()=> setDeleteModal(false)}
            onCancel={()=> setDeleteModal(false)}
            footer={[
              <Button key="back" type='danger' onClick={()=> setDeleteModal(false)}>
                Thoát
              </Button>,
              <Button key="submit" type="primary" onClick={()=> {
                dispatch(deleteTemplate(record.id, history))
                setDeleteModal(false)
                // window.location.reload()
              }}>
                Xóa
              </Button>,
            ]}
            centered
          >
            <Alert
              message="Bạn có chắc muốn xóa tài liệu mẫu này ?"
              type="warning"
              showIcon
            />
          </Modal>
        </Space>
      )
    }
  ]

  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra)
  }

  const handleClick = e => {

    switch (e.key) {
      // department
      case '2':
        if (window.location.pathname === '/admin') {
          history.push('/admin')
        } else history.push('/admin/department')
        break
      // doc
      case '3':
        history.push('/admin/template-management')
        break
      // user
      case '4':
        history.push('/admin/user-management')
        break;
      case '5':
        history.push('/admin/document-management')
        break
      default:
    }
  }

  if (auth.profile) console.log("LIST AUTH", auth.profile)
  if (auth.profile) console.log("LIST DEPARTMENT", department)
  return (
    <>
      {
        department.listDepartments &&
        <Row className="admin">
          <Col span={1} className="main-content">
            <div className='logo-wrapper'>
              {/* <SafetyOutlined className="logos" /> */}
              <Image className='logos' src={logo} 
                onClick={()=> {
                  history.push('/')
                }}
              />
            </div>
            <div className="question">
              <Tooltip key="edit" placement="right" color='blue' title={'Những câu hỏi thường gặp và giải đáp'}>
                <QuestionCircleTwoTone className="icon-left" onClick={
                  () => {
                    history.push('/faq')
                  }
                } />
              </Tooltip>
              {
                auth.profile !== null && auth.profile.avatar ?
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src={auth.profile.avatar}
                  />
                  :
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                  />
              }
            </div>
          </Col>

          <Col span={sidebarWidth} className="sidebar">
            <div className="full-height">
              <Menu
                defaultSelectedKeys={[defaultSelectMenuState]}
                // defaultOpenKeys={['sub1']}
                mode="inline"
                theme="light"
                inlineCollapsed={collapsed}
                className="full-height"
                onClick={handleClick}
              >
                <div className="collapse-wrapper">
                  <Button type="primary" onClick={toggleCollapsed} className="collapse">
                    {React.createElement(collapsed ? RightOutlined : LeftOutlined)}
                  </Button>
                </div>
                <Menu.Item key="1" icon={<UserOutlined />} disabled>
                  ADMIN
                </Menu.Item>
                <Menu.Item key="2" icon={<ApartmentOutlined />}>
                  Quản lý phòng ban
                </Menu.Item>
                <Menu.Item key="3" icon={<FileProtectOutlined />}>
                  Quản lý tài liệu mẫu
                </Menu.Item>
                <Menu.Item key="4" icon={<TeamOutlined />}>
                  Quản lý người dùng
                </Menu.Item>
                <Menu.Item key="5" icon={<FileWordOutlined />}>
                  Quản lý chung tài liệu
                </Menu.Item>

              </Menu>
            </div>
          </Col>

          <Col span={mainWidth} className="main">
            <div>
              <div className="top-title">
                <b>Danh sách tài liệu mẫu - {department.currentDepartment}</b>
                <Button type="primary" onClick={
                  ()=> {
                    dispatch(createTemplate(departmentId, history))
                  }
                }>Thêm mới tài liệu mẫu</Button>
              </div>
              <Table columns={columns} dataSource={department.listTemplates}
                onChange={onChange}
              />
              <Row justify="start">
                <Col>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      }
    </>
  )
}

// Index.Layout = AdminLayout

export default (Index)

