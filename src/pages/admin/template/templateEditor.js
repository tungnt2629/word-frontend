import React, { useCallback, useEffect, useState } from "react"
import { Button, Row, Col, Space, Image, Menu, Dropdown, Input, message, Alert } from 'antd'
import {
  ShareAltOutlined,
  FileWordTwoTone,
  StarOutlined,
  MessageOutlined,
  SaveOutlined,
  SyncOutlined,
  InfoCircleOutlined,
  LogoutOutlined
} from '@ant-design/icons'
import Quill from "quill"
import "quill/dist/quill.snow.css"
// import { io } from "socket.io-client"
import { useParams } from "react-router-dom"
import { useHistory } from "react-router"
import ApiService from "../../../services"
// import { showDocument } from "./actions/document"
import checkTokenAuth from "../../../services/routing"
import { useSelector, useDispatch } from 'react-redux'
import { viewProfile } from "../../../actions/auth"
import QuillCursors, { Cursor } from 'quill-cursors'
import { saveAs } from 'file-saver';
import * as quillToWord from 'quill-to-word'

const SAVE_INTERVAL_MS = 10000
const TOOLBAR_OPTIONS = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],

  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered' }, { 'list': 'bullet' }],
  [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction

  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean']                                         // remove formatting button
]

export default function TextEditor() {
  const currentPath = window.location.pathname
  const dispatch = useDispatch()
  const history = useHistory()

  const [title, setTitle] = useState('Loading ...')
  const { id: templateId } = useParams()
  const [quill, setQuill] = useState()
  const [loadingSave, setLoadingSave] = useState(false)

  const auth = useSelector(state => state.auth)

  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  useEffect(() => {
    if (!ApiService.getCookie('token')) {
      history.push('/login')
    }
  }, [])

  useEffect(() => {
    if (quill == null) return

    checkTokenAuth.getAuth(history)
    ApiService.get('/templates/show/' + templateId)
      .then((response) => {
        if (response.data.status === 200) {
          console.log("template show reesposne", response.data.data.title)
          setTitle(response.data.data.title)
          
          const delta = quill.clipboard.convert(response.data.data.content)
          quill.setContents(delta, 'silent')
          quill.enable()
        }
      })
  }, [quill])

  useEffect(() => {
    if (quill == null) return

    const interval = setInterval(() => {
      setLoadingSave(true)
        // let titleTmp = ''
        ApiService.get('/templates/show/' + templateId)
        .then((response) => {
          if (response.data.status === 200) {
            let searchParams = new URLSearchParams(window.location.search)
            const newTemplate = {
              title: title,
              content: quill.root.innerHTML,
              department_id: searchParams.get('department-id') || null
            }
            // console.log("--------------->==========>", newTemplate)
            if (newTemplate.department_id) {
              ApiService.post('/templates/update/' + templateId, newTemplate)
                .then((response) => {
                  if (response.status === 200) {
                    setLoadingSave(false)
                  } else {
                    history.push('/login')
                  }
                })
                .catch((error) => {
                })
            } else {
              message.error("Vui lòng chọn phòng ban muốn thêm tài liệu mẫu")
              history.push('/admin/template-management')
            }
          }
        })
    }, SAVE_INTERVAL_MS)

    return () => {
      clearInterval(interval)
    }
  }, [quill, title])

  const wrapperRef = useCallback(wrapper => {
    if (wrapper == null) return

    wrapper.innerHTML = ""
    const editor = document.createElement("div")
    wrapper.append(editor)
    Quill.register('modules/cursors', QuillCursors);
    const q = new Quill(editor, {
      theme: "snow",
      modules: {
        toolbar: TOOLBAR_OPTIONS,
        cursors: {
          template: '<div class="custom-cursor">...</div>',
          hideDelayMs: 5000,
          hideSpeedMs: 0,
          selectionChangeSource: null,
          transformOnTextChange: true,
        }
      }
    })
    q.disable()
    q.setText("Loading...")
    setQuill(q)
  }, [])

  const handleTitleChange = (e) => {
    setTitle(e.target.value)
  }

  const handlePressEnter = (e) => {
    let searchParams = new URLSearchParams(window.location.search)
    const newTemplate = {
      title: title,
      content: quill.root.innerHTML,
      department_id: searchParams.get('department-id') || null
    }
    if (newTemplate.department_id) {
      ApiService.post('/templates/update/' + templateId, newTemplate)
        .then((response) => {
          if (response.status === 200) {
            setLoadingSave(false)
          } else {
            history.push('/login')
          }
        })
        .catch((error) => {
        })
    } else {
      message.error("Vui lòng chọn phòng ban muốn thêm tài liệu mẫu")
      history.push('/admin/template-management')
    }
  } 

  const exportDocx = async () => {
    if(quill && title) {
      const delta = quill.getContents();
      const quillToWordConfig = {
          exportAs: 'blob'
      };
      const docAsBlob = await quillToWord.generateWord(delta, quillToWordConfig);
      saveAs(docAsBlob, title);
    }
  }

  const menu2 = (
    <Menu>
      <Menu.Item key={1}>
        <a rel="noopener noreferrer" onClick={()=> {
          exportDocx()
        }}>
          Chức nang 2
        </a>
      </Menu.Item>
    </Menu>
  )
  
  const menu = (
    <Menu>
      <Menu.Item key={1}>
        <a rel="noopener noreferrer" onClick={()=> {
          exportDocx()
        }}>
          Tải xuống (.docx)
        </a>
      </Menu.Item>
    </Menu>
  )

  const avatarMenu = (
    <Menu>
      <Menu.Item key='1' danger onClick={() => {
        ApiService.deleteAllCookies()
        history.push("/login")
      }}>
        <Space>
          <LogoutOutlined />
          Đăng xuất
        </Space>
      </Menu.Item>
      <Menu.Item key='2' onClick={() => {
        history.push("/about")
      }}>
        <Space>
          <InfoCircleOutlined />
          Thông tin cá nhân
        </Space>
      </Menu.Item>
      {
        auth.profile && auth.profile.role === 1 &&
        <Menu.Item key='3' onClick={() => {
          history.push("/admin")
        }}>
          <Space>
            <InfoCircleOutlined />
            Trang quản trị
          </Space>
        </Menu.Item>
      }
    </Menu>
  )
  // console.log(currentPath)
  return (
    <>
    {
      auth.profile &&
      <React.Fragment>
      <div className="header">
        <Row gutter={40} className="navbar" justify="space-between" align="middle">
          <Col>
            <Space >
              <FileWordTwoTone className="logo" onClick={() => {
                history.push('/')
              }}/>

              <div className="title-wrapper">
                <Space className="title-space">
                  <Input
                    // value={document.currentDocument.title}
                    value={title}
                    className="document-title"
                    onChange={e => handleTitleChange(e)}
                    onPressEnter={e => {
                      handlePressEnter(e)
                    }}
                  // onPressEnter={(e) => console.log(e.target.value)}
                  ></Input>
                  <Alert message="Bạn đang chỉnh sửa tài liệu mẫu" type="info" showIcon />
                  {
                    loadingSave === true ?
                      <>
                        <SyncOutlined spin />Đang lưu
                      </>
                      : 
                      <>
                        <SaveOutlined />Đã lưu
                      </>
                  }
                  {/* <CloudUploadOutlined /> */}
                </Space>
                
                <h4 className="document-subtitle">
                <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Tài liệu</Button>
                  </Dropdown>

                  {/* <Dropdown overlay={menu2} placement="bottomLeft" className="subtitle-menu">
                    <Button className="tool">Chỉnh sửa</Button>
                  </Dropdown> */}
                </h4>
              </div>
            </Space>
          </Col>
          <Col className="avatar-wrapper">
            {/* <Space> */}
            
            <Dropdown overlay={avatarMenu}>
              <Image
                alt="avatar"
                className="avatar"
                src={`https://ui-avatars.com/api/?name=${auth.profile.name}`}
              />
            </Dropdown>
            {/* </Space> */}
          </Col>
        </Row>
      </div>
      <div className="container" ref={wrapperRef}></div>

      {/* <div class="container">
        <div class="editor">
          <h2>User 1</h2>
          <center>(Expanding editor)</center>
          <br/>
          <div id="editor-one"></div>
        </div>

        <div class="editor">
          <h2>User 2</h2>
          <center>(Fixed-height editor)</center>
          <br/>
          <div id="editor-two"></div>
        </div>
      </div> */}
    </React.Fragment>
    }
    </>
  )
}
