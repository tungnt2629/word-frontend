import React, { useState } from 'react'
import {
  Row,
  Col,
  Image,
  Menu,
  Button,
  Table
} from 'antd'
import {
  ContainerOutlined,
  DesktopOutlined,
  LeftOutlined,
  QuestionCircleTwoTone,
  RightOutlined,
  SafetyOutlined
} from '@ant-design/icons'
import {
  AppstoreOutlined,
  MailOutlined
} from '@ant-design/icons';

function Sidebar() {
  const { SubMenu } = Menu

  const [sidebarWidth, setSidebarWidth] = useState(3)
  const [collapsed, setCollapsed] = useState(false)
  const [mainWidth, setMainWidth] = useState(20)

  const toggleCollapsed = () => {
    setCollapsed(!collapsed)

    if (!collapsed) {
      setSidebarWidth(1)
      setMainWidth(22)
    } else {
      setSidebarWidth(3)
      setMainWidth(20)
    }
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      filters: [
        {
          text: 'Joe',
          value: 'Joe'
        },
        {
          text: 'Category 1',
          value: 'Category 1',
          children: [
            {
              text: 'Yellow',
              value: 'Yellow'
            },
            {
              text: 'Pink',
              value: 'Pink'
            }
          ]
        },
        {
          text: 'Category 2',
          value: 'Category 2',
          children: [
            {
              text: 'Green',
              value: 'Green'
            },
            {
              text: 'Black',
              value: 'Black'
            }
          ]
        }
      ],
      filterMode: 'tree',
      filterSearch: true,
      onFilter: (value, record) => record.name.includes(value),
      width: '30%'
    },
    {
      title: 'Age',
      dataIndex: 'age',
      sorter: (a, b) => a.age - b.age
    },
    {
      title: 'Address',
      dataIndex: 'address',
      filters: [
        {
          text: 'London',
          value: 'London'
        },
        {
          text: 'New York',
          value: 'New York'
        },
      ],
      onFilter: (value, record) => record.address.startsWith(value),
      filterSearch: true,
      width: '40%'
    }
  ]

  const data = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      key: '4',
      name: 'Jim Red',
      age: 32,
      address: 'London No. 2 Lake Park'
    }
  ]

  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra);
  }

  return (
    <div>
      <Col span={1} className="main-content">
        <div>
          <SafetyOutlined className="logos"/>
        </div>
        <div className="question">
          <QuestionCircleTwoTone className="icon-left"/>
          <Image
            alt="avatar"
            className="icon-left"
            src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
          />
        </div>
      </Col>

      <Col span={ sidebarWidth } className="sidebar">
        <div className="full-height">
          <Menu
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
            theme="light"
            inlineCollapsed={collapsed}
            className="full-height"
          >
            <div className="collapse-wrapper">
              <Button type="primary" onClick={toggleCollapsed} className="collapse">
                {React.createElement(collapsed ? RightOutlined : LeftOutlined)}
              </Button>
            </div>
            <Menu.Item key="1">
              Option 1
            </Menu.Item>
            <Menu.Item key="2" icon={<DesktopOutlined />}>
              Option 2
            </Menu.Item>
            <Menu.Item key="3" icon={<ContainerOutlined />}>
              Option 3
            </Menu.Item>
            <SubMenu key="sub1" icon={<MailOutlined />} title="Navigation One">
              <Menu.Item key="5">Option 5</Menu.Item>
              <Menu.Item key="6">Option 6</Menu.Item>
              <Menu.Item key="7">Option 7</Menu.Item>
              <Menu.Item key="8">Option 8</Menu.Item>
            </SubMenu>
            <SubMenu key="sub2" icon={<AppstoreOutlined />} title="Navigation Two">
              <Menu.Item key="9">Option 9</Menu.Item>
              <Menu.Item key="10">Option 10</Menu.Item>
              <SubMenu key="sub3" title="Submenu">
                <Menu.Item key="11">Option 11</Menu.Item>
                <Menu.Item key="12">Option 12</Menu.Item>
              </SubMenu>
            </SubMenu>
          </Menu>
        </div>
      </Col>

      <Col span={ mainWidth } className="main">
        <div>Danh sách phòng ban</div>
        <Table columns={columns} dataSource={data} onChange={onChange} />
      </Col>
    </div>
  )
}

export default Sidebar
