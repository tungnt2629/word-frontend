import React, { useState } from 'react'
import {
  Row,
  Col,
  Image,
  Menu,
  Button,
  Table
} from 'antd'
import {
  ContainerOutlined,
  DesktopOutlined,
  LeftOutlined,
  QuestionCircleTwoTone,
  RightOutlined,
  SafetyOutlined
} from '@ant-design/icons'

const Layout = ({ children }) => {
  const [collapsed, setCollapsed] = useState(false)
  const [sidebarWidth, setSidebarWidth] = useState(3)
  const [mainWidth, setMainWidth] = useState(20)

  const toggleCollapsed = () => {
    setCollapsed(!collapsed)

    if (!collapsed) {
      setSidebarWidth(1)
      setMainWidth(22)
    } else {
      setSidebarWidth(3)
      setMainWidth(20)
    }
  }

  return (
    <>
      <Row className="admin">
        <Col span={1} className="main-content">
          <div>
            <SafetyOutlined className="logos" />
          </div>
          <div className="question">
            <QuestionCircleTwoTone className="icon-left" />
            <Image
              alt="avatar"
              className="icon-left"
              src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
            />
          </div>
        </Col>

        <Col span={sidebarWidth} className="sidebar">
          <div className="full-height">
            <Menu
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              mode="inline"
              theme="light"
              inlineCollapsed={collapsed}
              className="full-height"
            >
              <div className="collapse-wrapper">
                <Button type="primary" onClick={toggleCollapsed} className="collapse">
                  {React.createElement(collapsed ? RightOutlined : LeftOutlined)}
                </Button>
              </div>
              <Menu.Item key="1">
                Option 1
              </Menu.Item>
              <Menu.Item key="2" icon={<DesktopOutlined />}>
                Option 2
              </Menu.Item>
              <Menu.Item key="3" icon={<ContainerOutlined />}>
                Option 3
              </Menu.Item>
            </Menu>
          </div>
        </Col>

        <Col span={mainWidth} className="main">
          {children}
          aloalo
        </Col>
      </Row>
    </>
  )
}

export default Layout
