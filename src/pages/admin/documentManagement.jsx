import React, { useEffect, useState, Component } from 'react'
import {
  Row,
  Col,
  Image,
  Menu,
  Button,
  Table,
  Space,
  Modal,
  message,
  Input,
  Form,
  Layout,
  Icon,
  Tooltip,
  Alert
} from 'antd'
import {
  ApartmentOutlined,
  CheckOutlined,
  CloseOutlined,
  ContainerOutlined,
  DesktopOutlined,
  FileProtectOutlined,
  FileTextOutlined,
  FileWordOutlined,
  LeftOutlined,
  QuestionCircleTwoTone,
  RightOutlined,
  SafetyOutlined,
  TeamOutlined,
  UserOutlined
} from '@ant-design/icons'

import { useSelector, useDispatch } from 'react-redux'
import { createDepartment, deleteDepartment, listDepartment, updateDepartment } from '../../actions/department'
import { useHistory } from "react-router"
import moment from 'moment'
import { viewProfile } from '../../actions/auth'
import logo from '../../images/logo.png'
import { deleteDocument, deleteDocumentAdmin, listDocument, listDocumentAdmin } from '../../actions/document'


function Index() {
  const history = useHistory()
  const [collapsed, setCollapsed] = useState(false)
  const [sidebarWidth, setSidebarWidth] = useState(3)
  const [mainWidth, setMainWidth] = useState(20)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isModalVisible2, setIsModalVisible2] = useState(false)
  // const [viewState, setViewState] = useState()
  const [titleEditModal, setTitleEditModal] = useState()
  const [recordId, setRecordId] = useState(null)
  const [defaultSelectMenuState, setDefaultSelectMenuState] = useState('5')

  const document = useSelector(state => state.document)
  const auth = useSelector(state => state.auth)

  const dispatch = useDispatch()
  const toggleCollapsed = () => {
    setCollapsed(!collapsed)

    if (!collapsed) {
      setSidebarWidth(1)
      setMainWidth(22)
    } else {
      setSidebarWidth(3)
      setMainWidth(20)
    }
  }

  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  useEffect(() => {
    if(auth && auth.profile) {
      if(auth.profile.role !== 1) {
        message.error("Bạn không phải quản trị viên !")
        history.push("/")
      }
    }
  }, [auth])
  // const showModal = (name, id) => {
  //   setTitleEditModal(name)
  //   setRecordId(id)
  //   setIsModalVisible(true)
  // }

  // const handleOk = () => {
  //   setIsModalVisible(false)
  // }

  // const handleCancel = () => {
  //   setIsModalVisible(false)
  // }
  // list ra danh sách tài liệu
  // const showModal2 = () => {
  //   setIsModalVisible2(true)
  // }

  // const handleOk2 = () => {
  //   setIsModalVisible2(false)
  // }

  // const handleCancel2 = () => {
  //   setIsModalVisible2(false)
  // }

  useEffect(() => {
    async function fetchData() {
      dispatch(listDocumentAdmin(history))
    }
    fetchData()
  }, [])

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      width: '5%',
      render: (text, record) => (
        <>{record.id}</>
      )
    },
    {
      title: 'Thuộc phòng ban',
      dataIndex: ['department','name'],
      key: 'department'
    },
    {
      title: 'Tên tài liệu',
      dataIndex: 'title',
      key: 'title',
      render: (text, record) => (
        <b>{record.title}</b>
      )
    },
    {
      title: 'Nội dung',
      dataIndex: 'content',
      key: 'content',
      render: (text, record) => (
        <>{record.content.replace(/<[^>]+>/g, '').slice(0,200)}</>
      )
    },
    {
      title: 'Trạng thái chia sẻ',
      dataIndex: 'content',
      key: 'content',
      render: (text, record) => (
        <>{record.public === 1 ?
          <><CheckOutlined/> Tài liệu chia sẻ chung</>
          :
          <><CloseOutlined/> Tài liệu chưa được chia sẻ</>
        }</>
      )
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text, record) => (
        <>{moment(record.created_at).format('DD/MM/YYYY h:mm')}</>
      )
    },
    {
      title: 'Lần cuối chỉnh sửa',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: (text, record) => (
        <>{moment(record.updated_at).format('DD/MM/YYYY h:mm')}</>
      )
    },
    {
      title: 'Chức năng',
      dataIndex: 'functional',
      key: 'functional',
      width: '10%',
      render: (text, record) => (
        <Space>
          <Button type={'danger'} onClick={() => {
            setIsModalVisible(true)
          }}>
            {"Xóa"}
          </Button>
          <Modal
            title="Xóa tài liệu"
            visible={isModalVisible}
            onOk={()=> setIsModalVisible(false)}
            onCancel={()=> setIsModalVisible(false)}
            footer={[
              <Button key="back" type='danger' onClick={()=> setIsModalVisible(false)}>
                Thoát
              </Button>,
              <Button key="submit" type="primary" onClick={()=> {
                dispatch(deleteDocumentAdmin(record.id, history))
                setIsModalVisible(false)
              }}>
                Xóa
              </Button>,
            ]}
            centered
          >
            <Alert
              message="Bạn có chắc muốn xóa tài liệu này ?"
              type="warning"
              showIcon
            />
          </Modal>
        </Space>
      )
    }
  ]

  function onChange(pagination, filters, sorter, extra) {
  }

  const handleClick = e => {

    switch (e.key) {
      // department
      case '2':
        if (window.location.pathname === '/admin') {
          history.push('/admin')
        } else history.push('/admin/department')
        break
      // doc
      case '3':
        history.push('/admin/template-management')
        break
      // user
      case '4':
        history.push('/admin/user-management')
        break;
      case '5':
        history.push('/admin/document-management')
        break
      default:
    }
  }

  // if (auth.profile) console.log("LIST AUTH", auth.profile)
  // console.log("LIST DOCS", document.listDocuments)
  return (
    <>
      {
        document.listDocuments &&
        <Row className="admin">
          <Col span={1} className="main-content">
            <div className='logo-wrapper'>
              {/* <SafetyOutlined className="logos" /> */}
              <Image className='logos' src={logo} 
                onClick={()=> {
                  history.push('/')
                }}
              />
            </div>
            <div className="question">
              <Tooltip key="edit" placement="right" color='blue' title={'Những câu hỏi thường gặp và giải đáp'}>
                <QuestionCircleTwoTone className="icon-left" onClick={
                  () => {
                    history.push('/faq')
                  }
                } />
              </Tooltip>
              {
                auth.profile !== null && auth.profile.avatar ?
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src={auth.profile.avatar}
                  />
                  :
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                  />
              }
            </div>
          </Col>

          <Col span={sidebarWidth} className="sidebar">
            <div className="full-height">
              <Menu
                defaultSelectedKeys={[defaultSelectMenuState]}
                // defaultOpenKeys={['sub1']}
                mode="inline"
                theme="light"
                inlineCollapsed={collapsed}
                className="full-height"
                onClick={handleClick}
              >
                <div className="collapse-wrapper">
                  <Button type="primary" onClick={toggleCollapsed} className="collapse">
                    {React.createElement(collapsed ? RightOutlined : LeftOutlined)}
                  </Button>
                </div>
                <Menu.Item key="1" icon={<UserOutlined />} disabled>
                  ADMIN
                </Menu.Item>
                <Menu.Item key="2" icon={<ApartmentOutlined />} defaultChecked>
                  Quản lý phòng ban
                </Menu.Item>
                <Menu.Item key="3" icon={<FileProtectOutlined />}>
                  Quản lý tài liệu mẫu
                </Menu.Item>
                <Menu.Item key="4" icon={<TeamOutlined />}>
                  Quản lý người dùng
                </Menu.Item>
                <Menu.Item key="5" icon={<FileWordOutlined />}>
                  Quản lý chung tài liệu
                </Menu.Item>

              </Menu>
            </div>
          </Col>

          <Col span={mainWidth} className="main">
            <div>
              <div className="top-title">
                <b>Danh sách tài liệu</b>
                {/* <Button type="primary" onClick={showModal2}>Thêm mới tài liệu</Button> */}
              </div>
              <Table columns={columns} dataSource={document.listDocuments}
                onChange={onChange}
              />
              <Row justify="start">
                <Col>
                  {/* new department modal*/}
                  {/* <Modal
                    visible={isModalVisible2}
                    title="Tạo tài liệu mới"
                    footer={[
                    ]}
                    onOk={handleOk2}
                    onCancel={handleCancel2}
                  >
                    <Form
                      id="createForm"
                      name="createForm"
                      onFinish={(values) => {
                        // console.log('new department:', values)
                        dispatch(createDepartment(values, history))
                      }}
                      onOk={handleOk2}
                    >
                      <Form.Item
                        label="Tên tài liệu"
                        name="name"
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng nhập tên tài liệu!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                      <Form.Item
                        wrapperCol={{
                          offset: 8,
                          span: 16,
                        }}
                      >
                        <Space>
                          <Button key="save" onClick={handleCancel2} type='danger'>
                            Thoát
                          </Button>
                          <Button form="createForm" key="create_submit" htmlType='submit' type={'primary'}>
                            Lưu
                          </Button>
                        </Space>
                      </Form.Item>
                    </Form>
                  </Modal> */}
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      }
    </>
  )
}

// Index.Layout = AdminLayout

export default (Index)

