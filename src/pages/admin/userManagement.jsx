import React, { useEffect, useState, Component } from 'react'
import {
  Row,
  Col,
  Image,
  Menu,
  Button,
  Table,
  Space,
  Modal,
  message,
  Input,
  Form,
  Layout,
  Icon,
  Tooltip,
  Alert,
  DatePicker,
  Select
} from 'antd'
import {
  ApartmentOutlined,
  CheckCircleOutlined,
  ContainerOutlined,
  DesktopOutlined,
  FileProtectOutlined,
  FileTextOutlined,
  FileWordOutlined,
  LeftOutlined,
  LockOutlined,
  MailOutlined,
  PhoneOutlined,
  QuestionCircleTwoTone,
  RightOutlined,
  SafetyOutlined,
  TeamOutlined,
  UserOutlined
} from '@ant-design/icons'

import { useSelector, useDispatch } from 'react-redux'
import { createDepartment, deleteDepartment, listDepartment, updateDepartment } from '../../actions/department'
import { useHistory } from "react-router"
import moment from 'moment'
import { addUser, register, setErrorMessage, viewProfile } from '../../actions/auth'
import logo from '../../images/logo.png'
import { createUser, deleteUser, listUser, updateUser } from '../../actions/user'


function Index() {
  const history = useHistory()
  const [editUserForm] = Form.useForm()

  const { Option } = Select;
  const [collapsed, setCollapsed] = useState(false)
  const [sidebarWidth, setSidebarWidth] = useState(3)
  const [mainWidth, setMainWidth] = useState(20)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isModalVisible2, setIsModalVisible2] = useState(false)
  // const [viewState, setViewState] = useState()
  const [titleEditModal, setTitleEditModal] = useState()
  const [recordId, setRecordId] = useState(null)
  const [recordEdit, setRecordEdit] = useState({

  })
  const [defaultSelectMenuState, setDefaultSelectMenuState] = useState('4')
  const [deleteModal, setDeleteModal] = useState(false)

  const user = useSelector(state => state.user)
  const auth = useSelector(state => state.auth)
  const department = useSelector(state => state.department)

  const dispatch = useDispatch()
  const toggleCollapsed = () => {
    setCollapsed(!collapsed)

    if (!collapsed) {
      setSidebarWidth(1)
      setMainWidth(22)
    } else {
      setSidebarWidth(3)
      setMainWidth(20)
    }
  }

  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  useEffect(() => {
    if(auth && auth.profile) {
      if(auth.profile.role !== 1) {
        message.error("Bạn không phải quản trị viên !")
        history.push("/")
      }
    }
  }, [auth])
  
  useEffect(() => {
    async function fetchData() {
      dispatch(listDepartment(history))
    }
    fetchData()
  }, [])
  
  const showModal = (name, id, record) => {
    setTitleEditModal(name)
    setRecordId(id)
    setIsModalVisible(true)
    setRecordEdit(record)
  }
  useEffect(() => {
    if (Object.keys(recordEdit).length !== 0) {
      editUserForm.setFieldsValue({
        name: recordEdit.name,
        gender: recordEdit.gender,
        dob: moment(recordEdit.dob, 'DD/MM/YYYY hh:mm'),
        phone: recordEdit.phone,
        email: recordEdit.email,
        department_id: recordEdit.department.name
      })
    }
   }, [editUserForm, recordEdit])



  const handleOk = () => {
    setIsModalVisible(false)
    setRecordId(null)
  }

  const handleCancel = () => {
    setIsModalVisible(false)
    setRecordId(null)
  }

  const showModal2 = () => {
    setIsModalVisible2(true)
  }

  const handleOk2 = () => {
    setIsModalVisible2(false)
  }

  const handleCancel2 = () => {
    setIsModalVisible2(false)
  }

  useEffect(() => {
    async function fetchData() {
      dispatch(listUser(history))
    }
    fetchData()
  }, [])

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      width: '5%',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>{record.id}</>
        }</>
      )
    },
    {
      title: 'Hình đại diện',
      dataIndex: 'id',
      key: 'id',
      width: '10%',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <Image src={record.avatar}/>
        }</>
      )
    },
    {
      title: 'Tên người dùng',
      dataIndex: 'name',
      key: 'name',
      // width: '30%',
      render: (text, record) => (
        <>{
          record.role === 0 &&
            <Space>
              <b>{record.name}</b>
              {/* <CheckCircleOutlined style={{color:'green'}} /> */}
            </Space>
        }</>
      )
    },
    {
      title: 'Phòng ban',
      // dataIndex: 'department',
      key: 'department',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <b>{record.department.name}</b>
        }</>
      )
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'dob',
      key: 'dob',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>{record.dob.split(" ")[0]}</>
        }</>
      )
    },
    {
      title: 'Giới tính',
      dataIndex: 'gender',
      key: 'gender',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>
          {
            record.gender === 'nam' ?
            <>Nam</>
            :<>Nữ</>
          }</>
        }</>
      )
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>{record.email}</>
        }</>
      )
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      key: 'phone',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>{record.phone}</>
        }</>
      )
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>{moment(record.created_at).format('DD/MM/YYYY h:mm')}</>
        }</>
      )
    },
    {
      title: 'Lần cuối chỉnh sửa',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>{moment(record.updated_at).format('DD/MM/YYYY h:mm')}</>
        }</>
      )
    },
    {
      title: 'Chức năng',
      dataIndex: 'functional',
      key: 'functional',
      width: '10%',
      render: (text, record) => (
        <>{
          record.role === 0 &&
          <>
            <Space>
              <Button type={'primary'} onClick={() => {
                showModal(record.name, record.id, record)
                dispatch(setErrorMessage(''))
                // console.log(recordEdit)
              }}>
                {'Sửa'}
              </Button>

              <Modal
                style={{top:20}}
                visible={isModalVisible}
                title={`Chỉnh sửa người dùng: ${titleEditModal} (ID: ${recordId})`}
                footer={[
                ]}
                onOk={handleOk}
                onCancel={handleCancel}
              >
                <div className="form">
                  {
                    auth.error &&
                    <Alert
                      message={auth.error}
                      type="error"
                      showIcon
                    />
                  }
                  {
                    Object.keys(recordEdit).length !== 0 &&
                    <Form
                      name="basic"
                      onFinish={(values) => {
                        values['dob'] = (moment(values.dob).format('DD/MM/YYYY h:mm'))
                        console.log(values, recordId)
                        dispatch(updateUser(values, recordId, history))
                        // setIsModalVisible(false)
                        // dispatch(createUser(values, history))
                      }}
                      onOk={handleOk}
                      autoComplete="off"
                      initialValues={{
                        name: recordEdit.name,
                        gender: recordEdit.gender,
                        dob: moment(recordEdit.dob, 'DD/MM/YYYY hh:mm'),
                        phone: recordEdit.phone,
                        email: recordEdit.email,
                        department_id: recordEdit.department.name
                        // ["password"]: recordEdit.password,
                      }}
                      // initialValues={recordEdit}
                      form={editUserForm}
                    >
                      <span>Họ tên</span>
                      <Form.Item
                        name="name"
                        rules={[
                          {
                            required: true,
                            message: 'Họ tên không được để trống'
                          }
                        ]}
                      >
                        <Input addonAfter={<UserOutlined />} />
                      </Form.Item>

                      <span>Giới tính</span>
                      <Form.Item
                        name="gender"
                        rules={[
                          {
                            required: true,
                            message: 'Giới tính không được để trống'
                          }
                        ]}
                      >
                        <Select style={{ width: '100%' }}>
                          <Option value="nam">Nam</Option>
                          <Option value="nu">Nữ</Option>
                        </Select>
                      </Form.Item>

                      <span>Thuộc phòng ban</span>
                      <Form.Item
                        name="department_id"
                        rules={[
                          {
                            required: true,
                            message: 'Bạn cần lựa chọn phòng ban'
                          }
                        ]}
                      >
                        <Select defaultValue="" style={{ width: '100%' }}>
                          {
                            department.listDepartments.length !== 0 &&
                            <>
                              {
                                department.listDepartments.map((item, key) => (
                                  <Option key={key+1} value={item.id}>{item.name}</Option>
                                ))
                              }
                            </>
                          }
                        </Select>
                      </Form.Item>

                      <span>Ngày sinh</span>
                      <Form.Item
                        name="dob"
                        rules={[
                          {
                            required: true,
                            message: 'Ngày sinh không được để trống'
                          }
                        ]}
                      >
                        <DatePicker
                          disabledDate={disabledDate}
                          className="date-picker"
                        />
                      </Form.Item>

                      <span>Số điện thoại</span>
                      <Form.Item
                        name="phone"
                        rules={[
                          {
                            required: true,
                            message: 'Số điện thoại không được để trống'
                          }
                        ]}
                      >
                        <Input addonAfter={<PhoneOutlined />} />
                      </Form.Item>

                      <span>Email</span>
                      <Form.Item
                        name="email"
                        rules={[
                          {
                            required: true,
                            message: 'Email không được để trống'
                          },
                          {
                            type: 'email',
                            message: 'Email không đúng định dạng'
                          }
                        ]}
                      >
                        <Input addonAfter={<MailOutlined />} />
                      </Form.Item>

                       <span>Mật khẩu</span>
                      <Form.Item
                        name="password"
                        rules={[
                          {
                            required: true,
                            message: 'Mật khẩu không được để trống'
                          },
                          {
                            min: 8,
                            message: 'Mật khẩu tối thiểu 8 ký tự'
                          },
                          {
                            max: 32,
                            message: 'Mật khẩu tối đa 32 ký tự'
                          }
                        ]}
                      >
                        <Input.Password type="password" addonAfter={<LockOutlined />} />
                      </Form.Item>

                      {/*<span>Nhập lại mật khẩu</span>
                      <Form.Item
                        name="confirm"
                        dependencies={['password']}
                        rules={[
                          {
                            required: true,
                            message: 'Mật khẩu không được để trống'
                          },
                          {
                            min: 8,
                            message: 'Mật khẩu tối thiểu 8 ký tự'
                          },
                          {
                            max: 32,
                            message: 'Mật khẩu tối đa 32 ký tự'
                          },
                          ({ getFieldValue }) => ({
                            validator(_, value) {
                              if (!value || getFieldValue('password') === value) {
                                return Promise.resolve()
                              }
                              return Promise.reject(new Error('Mật khẩu nhập lại không khớp!'))
                            }
                          })
                        ]}
                      >
                        <Input.Password type="password" addonAfter={<LockOutlined />} />
                      </Form.Item> */}

                      {/* <div className='forgot' onClick={()=> router.push('/user/forgot')}><a>Quên mật khẩu</a></div> */}
                      <Form.Item
                        wrapperCol={{
                          offset: 8,
                          span: 16,
                        }}
                      >
                        <Space>
                          <Button
                            // loading={auth.isLoading}
                            danger
                            onClick={handleCancel}
                            style={{ marginTop: 20 }}
                          >
                            Thoát
                          </Button>
                          <Button
                            // loading={auth.isLoading}
                            type="primary"
                            htmlType="submit"
                            style={{ marginTop: 20 }}
                          >
                            Sửa
                          </Button>
                        </Space>
                      </Form.Item>
                    </Form>
                  }
                </div>
              </Modal>

              <Button type={'danger'} onClick={() => {
                setDeleteModal(true)
              }}>
                {"Xóa"}
              </Button>
              <Modal
                title="Xóa tài liệu"
                visible={deleteModal}
                onOk={()=> setDeleteModal(false)}
                onCancel={()=> setDeleteModal(false)}
                footer={[
                  <Button key="back" type='danger' onClick={()=> setDeleteModal(false)}>
                    Thoát
                  </Button>,
                  <Button key="submit" type="primary" onClick={()=> {
                    dispatch(deleteUser(record.id, history))
                    setDeleteModal(false)
                  }}>
                    Xóa
                  </Button>,
                ]}
                centered
              >
                <Alert
                  message="Bạn có chắc muốn xóa người dùng này ?"
                  type="warning"
                  showIcon
                />
              </Modal>
            </Space>
          </>
        }</>
      )
    }
  ]

  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra)
  }

  const handleClick = e => {

    switch (e.key) {
      // department
      case '2':
        if (window.location.pathname === '/admin') {
          history.push('/admin')
        } else history.push('/admin/department')
        break
      // doc
      case '3':
        history.push('/admin/template-management')
        break
      // user
      case '4':
        history.push('/admin/user-management')
        break;
      case '5':
        history.push('/admin/document-management')
        break
      default:
    }
  }

  function disabledDate(current) {
    // Can not select days before today and today
    return current && current > moment().endOf('day');
  }

  // if (auth.profile) console.log("LIST AUTH", auth.profile)
  // if (auth.profile) console.log("LIST DEPARTMENT", user.listUsers)
  // console.warn("department", department.listDepartments)/
  return (
    <>
      {
        user.listUsers &&
        <Row className="admin">
          <Col span={1} className="main-content">
            <div className='logo-wrapper'>
              {/* <SafetyOutlined className="logos" /> */}
              <Image className='logos' src={logo} 
                onClick={()=> {
                  history.push('/')
                }}
              />
            </div>
            <div className="question">
              <Tooltip key="edit" placement="right" color='blue' title={'Những câu hỏi thường gặp và giải đáp'}>
                <QuestionCircleTwoTone className="icon-left" onClick={
                  () => {
                    history.push('/faq')
                  }
                } />
              </Tooltip>
              {
                auth.profile !== null && auth.profile.avatar ?
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src={auth.profile.avatar}
                  />
                  :
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                  />
              }
            </div>
          </Col>

          <Col span={sidebarWidth} className="sidebar">
            <div className="full-height">
              <Menu
                defaultSelectedKeys={[defaultSelectMenuState]}
                // defaultOpenKeys={['sub1']}
                mode="inline"
                theme="light"
                inlineCollapsed={collapsed}
                className="full-height"
                onClick={handleClick}
              >
                <div className="collapse-wrapper">
                  <Button type="primary" onClick={toggleCollapsed} className="collapse">
                    {React.createElement(collapsed ? RightOutlined : LeftOutlined)}
                  </Button>
                </div>
                <Menu.Item key="1" icon={<UserOutlined />} disabled>
                  ADMIN
                </Menu.Item>
                <Menu.Item key="2" icon={<ApartmentOutlined />} defaultChecked>
                  Quản lý phòng ban
                </Menu.Item>
                <Menu.Item key="3" icon={<FileProtectOutlined />}>
                  Quản lý tài liệu mẫu
                </Menu.Item>
                <Menu.Item key="4" icon={<TeamOutlined />}>
                  Quản lý người dùng
                </Menu.Item>
                <Menu.Item key="5" icon={<FileWordOutlined />}>
                  Quản lý chung tài liệu
                </Menu.Item>

              </Menu>
            </div>
          </Col>

          <Col span={mainWidth} className="main">
            <div>
              <div className="top-title">
                <b>Danh sách người dùng</b>
                <Button type="primary" onClick={showModal2}>Thêm mới người dùng</Button>
              </div>
              <Table columns={columns} dataSource={user.listUsers}
                onChange={onChange}
              />
              <Row justify="start">
                <Col>
                  {/* new department modal*/}
                  <Modal
                    style={{ top: 20 }}
                    visible={isModalVisible2}
                    title="Tạo người dùng mới"
                    footer={[
                    ]}
                    onOk={handleOk2}
                    onCancel={handleCancel2}
                  >
                    <div className="form">
                      {
                        auth.error &&
                        <Alert
                          message={auth.error}
                          type="error"
                          showIcon
                        />
                      }
                      <Form
                        name="basic"
                        onFinish={(values) => {
                          values['dob'] = (moment(values.dob).format('DD/MM/YYYY h:mm'))
                          values['avatar'] = `https://ui-avatars.com/api/?name=${values.name}`
                          // console.log(values)
                          dispatch(addUser(values, history))
                          setIsModalVisible2(false)
                          // dispatch(createUser(values, history))
                        }}
                        // onOk={handleOk2}
                        autoComplete="off"
                      >
                        <span>Họ tên</span>
                        <Form.Item
                          name="name"
                          rules={[
                            {
                              required: true,
                              message: 'Họ tên không được để trống'
                            }
                          ]}
                        >
                          <Input addonAfter={<UserOutlined />} />
                        </Form.Item>

                        <span>Giới tính</span>
                        <Form.Item
                          name="gender"
                          rules={[
                            {
                              required: true,
                              message: 'Giới tính không được để trống'
                            }
                          ]}
                        >
                          <Select defaultValue="" style={{ width: '100%' }}>
                            <Option key='1' value="nam">Nam</Option>
                            <Option key='2' value="nu">Nữ</Option>
                          </Select>
                        </Form.Item>
                       
                        <span>Thuộc phòng ban</span>
                        <Form.Item
                          name="department_id"
                          rules={[
                            {
                              required: true,
                              message: 'Bạn cần lựa chọn phòng ban'
                            }
                          ]}
                        >
                          <Select defaultValue="" style={{ width: '100%' }}>
                            {
                              department.listDepartments.length !== 0 &&
                              <>
                                {
                                  department.listDepartments.map((item, key) => (
                                    <Option key={key+1} value={item.id}>{item.name}</Option>
                                  ))
                                }
                              </>
                            }
                          </Select>
                        </Form.Item>

                        <span>Ngày sinh</span>
                        <Form.Item
                          name="dob"
                          rules={[
                            {
                              required: true,
                              message: 'Ngày sinh không được để trống'
                            }
                          ]}
                        >
                          <DatePicker
                            disabledDate={disabledDate}
                            className="date-picker"
                          />
                        </Form.Item>

                        <span>Số điện thoại</span>
                        <Form.Item
                          name="phone"
                          rules={[
                            {
                              required: true,
                              message: 'Số điện thoại không được để trống'
                            }
                          ]}
                        >
                          <Input addonAfter={<PhoneOutlined />} />
                        </Form.Item>

                        <span>Email</span>
                        <Form.Item
                          name="email"
                          rules={[
                            {
                              required: true,
                              message: 'Email không được để trống'
                            },
                            {
                              type: 'email',
                              message: 'Email không đúng định dạng'
                            }
                          ]}
                        >
                          <Input addonAfter={<MailOutlined />} />
                        </Form.Item>

                        <span>Mật khẩu</span>
                        <Form.Item
                          name="password"
                          rules={[
                            {
                              required: true,
                              message: 'Mật khẩu không được để trống'
                            },
                            {
                              min: 8,
                              message: 'Mật khẩu tối thiểu 8 ký tự'
                            },
                            {
                              max: 32,
                              message: 'Mật khẩu tối đa 32 ký tự'
                            }
                          ]}
                        >
                          <Input.Password type="password" addonAfter={<LockOutlined />} />
                        </Form.Item>

                        <span>Nhập lại mật khẩu</span>
                        <Form.Item
                          name="confirm"
                          dependencies={['password']}
                          rules={[
                            {
                              required: true,
                              message: 'Mật khẩu không được để trống'
                            },
                            {
                              min: 8,
                              message: 'Mật khẩu tối thiểu 8 ký tự'
                            },
                            {
                              max: 32,
                              message: 'Mật khẩu tối đa 32 ký tự'
                            },
                            ({ getFieldValue }) => ({
                              validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                  return Promise.resolve()
                                }
                                return Promise.reject(new Error('Mật khẩu nhập lại không khớp!'))
                              }
                            })
                          ]}
                        >
                          <Input.Password type="password" addonAfter={<LockOutlined />} />
                        </Form.Item>

                        {/* <div className='forgot' onClick={()=> router.push('/user/forgot')}><a>Quên mật khẩu</a></div> */}
                        <Form.Item
                          wrapperCol={{
                            offset: 8,
                            span: 16,
                          }}
                        >
                          <Space>
                            <Button
                              // loading={auth.isLoading}
                              danger
                              onClick={handleCancel2}
                              style={{ marginTop: 20 }}
                            >
                              Thoát
                            </Button>
                            <Button
                              // loading={auth.isLoading}
                              type="primary"
                              htmlType="submit"
                              style={{ marginTop: 20 }}
                              onClick={()=>{
                                setIsModalVisible2(true)
                              }}
                            >
                              Thêm mới
                            </Button>
                          </Space>
                        </Form.Item>
                      </Form>
                    </div>
                  </Modal>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      }
    </>
  )
}

// Index.Layout = AdminLayout

export default (Index)

