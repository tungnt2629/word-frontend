import React, { useEffect, useState } from 'react'
import {
  Row,
  Col,
  Image,
  Menu,
  Button,
  Table,
  Space,
  Modal,
  Input,
  Form,
  Tooltip,
  Alert,
  Divider,
  Empty,
  message
} from 'antd'
import {
  ApartmentOutlined,
  EditOutlined,
  EyeOutlined,
  FileProtectOutlined,
  FileWordOutlined,
  LeftOutlined,
  PaperClipOutlined,
  QuestionCircleTwoTone,
  RightOutlined,
  TeamOutlined,
  UserOutlined
} from '@ant-design/icons'

import { useSelector, useDispatch } from 'react-redux'
import { createDepartment, deleteDepartment, listDepartment, updateDepartment } from '../../actions/department'
import { useHistory } from "react-router"
import moment from 'moment'
import { viewProfile } from '../../actions/auth'
import logo from '../../images/logo.png'


function Index() {
  const history = useHistory()
  const [collapsed, setCollapsed] = useState(false)
  const [sidebarWidth, setSidebarWidth] = useState(3)
  const [mainWidth, setMainWidth] = useState(20)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isModalVisible2, setIsModalVisible2] = useState(false)
  // const [viewState, setViewState] = useState()
  const [titleEditModal, setTitleEditModal] = useState()
  const [recordId, setRecordId] = useState(null)
  const [defaultSelectMenuState, setDefaultSelectMenuState] = useState('2')
  const [deleteModal, setDeleteModal] = useState(false)
  const [listUserModal, setListUserModal] = useState(false)
  const [listUserDetail, setListUserDetail] = useState([])

  const department = useSelector(state => state.department)
  const auth = useSelector(state => state.auth)

  const dispatch = useDispatch()
  const toggleCollapsed = () => {
    setCollapsed(!collapsed)

    if (!collapsed) {
      setSidebarWidth(1)
      setMainWidth(22)
    } else {
      setSidebarWidth(3)
      setMainWidth(20)
    }
  }

  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  useEffect(() => {
    if(auth && auth.profile) {
      if(auth.profile.role !== 1) {
        message.error("Bạn không phải quản trị viên !")
        history.push("/")
      }
    }
  }, [auth])

  const showModal = (name, id) => {
    setTitleEditModal(name)
    setRecordId(id)
    setIsModalVisible(true)
  }
  const showModalListUser = (name) => {
    setTitleEditModal(name)
    // setRecordId(id)
    setListUserModal(true)
  }

  const handleOk = () => {
    setIsModalVisible(false)
  }

  const handleCancel = () => {
    setIsModalVisible(false)
  }

  const handleOkListUser = () => {
    setListUserModal(false)
  }

  const handleCancelListUser = () => {
    setListUserModal(false)
  }
  // list ra danh sách phòng ban
  const showModal2 = () => {
    setIsModalVisible2(true)
  }

  const handleOk2 = () => {
    setIsModalVisible2(false)
  }

  const handleCancel2 = () => {
    setIsModalVisible2(false)
  }

  useEffect(() => {
    async function fetchData() {
      dispatch(listDepartment(history))
    }
    fetchData()
  }, [])

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      width: '5%',
      render: (text, record) => (
        <>{record.id}</>
      )
    },
    {
      title: 'Tên phòng ban',
      // dataIndex: 'name',
      key: 'name',
      width: '30%',
      render: (text, record) => (
        <b>{record.name}</b>
      )
    },
    {
      title: 'Danh sách nhân viên',
      // dataIndex: 'name',
      key: 'name',
      render: (text, record) => (
        <>
          <Button type={'primary'} onClick={() => {
            showModalListUser(record.name)
            setListUserDetail(record.users)
          }}>
            <EyeOutlined /> Xem danh sách nhân viên
          </Button>
        </>
      )
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text, record) => (
        <>{moment(record.created_at).format('DD/MM/YYYY h:mm')}</>
      )
    },
    {
      title: 'Lần cuối chỉnh sửa',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: (text, record) => (
        <>{moment(record.updated_at).format('DD/MM/YYYY h:mm')}</>
      )
    },
    {
      title: 'Chức năng',
      dataIndex: 'functional',
      key: 'functional',
      width: '10%',
      render: (text, record) => (
        <Space>
          <Button type={'primary'} onClick={() => {
            showModal(record.name, record.id)
          }}>
            {'Sửa'}
          </Button>

          <Modal
            visible={isModalVisible}
            title={<><EditOutlined /> Chỉnh sửa phòng ban {titleEditModal}</>}
            footer={[
            ]}
            onOk={handleOk}
            onCancel={handleCancel}
            centered
          >
            <Form
              id="editForm"
              name="editForm"
              onFinish={
                (values) => {
                  console.log('Edited department:', values, recordId)
                  if (recordId) {
                    dispatch(updateDepartment(values, recordId, history))
                  }
                }
              }
              // initialValues={{
              //   name: titleEditModal
              // }}
            >
              <Form.Item
                label={<>Chỉnh sửa phòng ban </>}
                name="name"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập tên phòng ban!',
                  },
                ]}
              >
                <Input placeholder={titleEditModal}/>
              </Form.Item>
              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Space>
                  <Button type='danger' onClick={handleCancel}>
                    Thoát
                  </Button>
                  <Button type="primary" htmlType="submit" onClick={() => {
                    setIsModalVisible(false)
                  }}>
                    Sửa
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          </Modal>

          <Button type={'danger'} onClick={() => {
            setDeleteModal(true)
          }}>
            {"Xóa"}
          </Button>
          <Modal
            title="Xóa phòng ban"
            visible={deleteModal}
            onOk={()=> setDeleteModal(false)}
            onCancel={()=> setDeleteModal(false)}
            footer={[
              <Button key="back" type='danger' onClick={()=> setDeleteModal(false)}>
                Thoát
              </Button>,
              <Button key="submit" type="primary" onClick={()=> {
                dispatch(deleteDepartment(record.id, history))
                setDeleteModal(false)
              }}>
                Xóa
              </Button>,
            ]}
            centered
          >
            <Alert
              message="Bạn có chắc muốn xóa phòng này ?"
              type="warning"
              showIcon
            />
          </Modal>
        </Space>
      )
    }
  ]

  console.warn(listUserDetail)
  function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra)
  }

  const handleClick = e => {

    switch (e.key) {
      // department
      case '2':
        if (window.location.pathname === '/admin') {
          history.push('/admin')
        } else history.push('/admin/department')
        break
      // doc
      case '3':
        history.push('/admin/template-management')
        break
      // user
      case '4':
        history.push('/admin/user-management')
        break;
      case '5':
        history.push('/admin/document-management')
        break
      default:
    }
  }

  if (auth.profile) console.log("LIST AUTH", auth.profile)
  if (auth.profile) console.log("LIST DEPARTMENT", department.listDepartments)
  return (
    <>
      {
        department.listDepartments &&
        <Row className="admin">
          <Col span={1} className="main-content">
            <div className='logo-wrapper'>
              {/* <SafetyOutlined className="logos" /> */}
              <Image className='logos' src={logo} 
                onClick={()=> {
                  history.push('/')
                }}
              />
            </div>
            <div className="question">
              <Tooltip key="edit" placement="right" color='blue' title={'Những câu hỏi thường gặp và giải đáp'}>
                <QuestionCircleTwoTone className="icon-left" onClick={
                  () => {
                    history.push('/faq')
                  }
                } />
              </Tooltip>
              {
                auth.profile !== null && auth.profile.avatar ?
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src={auth.profile.avatar}
                  />
                  :
                  <Image
                    alt="avatar"
                    className="icon-left"
                    src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                  />
              }
            </div>
          </Col>

          <Col span={sidebarWidth} className="sidebar">
            <div className="full-height">
              <Menu
                defaultSelectedKeys={[defaultSelectMenuState]}
                // defaultOpenKeys={['sub1']}
                mode="inline"
                theme="light"
                inlineCollapsed={collapsed}
                className="full-height"
                onClick={handleClick}
              >
                <div className="collapse-wrapper">
                  <Button type="primary" onClick={toggleCollapsed} className="collapse">
                    {React.createElement(collapsed ? RightOutlined : LeftOutlined)}
                  </Button>
                </div>
                <Menu.Item key="1" icon={<UserOutlined />} disabled>
                  ADMIN
                </Menu.Item>
                <Menu.Item key="2" icon={<ApartmentOutlined />} defaultChecked>
                  Quản lý phòng ban
                </Menu.Item>
                <Menu.Item key="3" icon={<FileProtectOutlined />}>
                  Quản lý tài liệu mẫu
                </Menu.Item>
                <Menu.Item key="4" icon={<TeamOutlined />}>
                  Quản lý người dùng
                </Menu.Item>
                <Menu.Item key="5" icon={<FileWordOutlined />}>
                  Quản lý chung tài liệu
                </Menu.Item>

              </Menu>
            </div>
          </Col>

          <Col span={mainWidth} className="main">
            <div>
              <div className="top-title">
                <b>Danh sách phòng ban</b>
                <Button type="primary" onClick={showModal2}>Thêm mới phòng ban</Button>
              </div>
              <Table columns={columns} dataSource={department.listDepartments}
                onChange={onChange}
              />
              <Row justify="start">
                <Col>
                  {/* new department modal*/}
                  <Modal
                    visible={isModalVisible2}
                    title="Tạo phòng ban mới"
                    footer={[
                    ]}
                    onOk={handleOk2}
                    onCancel={handleCancel2}
                    centered
                  >
                    <Form
                      id="createForm"
                      name="createForm"
                      onFinish={(values) => {
                        // console.log('new department:', values)
                        dispatch(createDepartment(values, history))
                      }}
                      onOk={handleOk2}
                    >
                      <Form.Item
                        label="Tên phòng ban"
                        name="name"
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng nhập tên phòng ban!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                      <Form.Item
                        wrapperCol={{
                          offset: 8,
                          span: 16,
                        }}
                      >
                        <Space>
                          <Button key="save" onClick={handleCancel2} type='danger'>
                            Thoát
                          </Button>
                          <Button form="createForm" key="create_submit" htmlType='submit' type={'primary'} onClick={()=> {
                            setIsModalVisible2(false)
                          }}>
                            Lưu
                          </Button>
                        </Space>
                      </Form.Item>
                    </Form>
                  </Modal>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      }
      <Modal
        visible={listUserModal}
        title={<><PaperClipOutlined/> Danh sách nhân viên thuộc {titleEditModal}</>}
        footer={[
          <Button type='primary'><a href="/admin/user-management">Truy cập trang quản lý người dùng</a></Button>
        ]}
        onOk={handleOkListUser}
        onCancel={handleCancelListUser}
        centered
      >
        {
          listUserDetail.length !== 0 ?
          <>
            {
              listUserDetail.map((item, key) => (
                <Row justify='space-between' align='middle' key={key+1}>
                  <Col><Image src={item.avatar}></Image></Col>
                  <Col>{item.name}</Col>
                  <Col>{item.email}</Col>
                  <Divider/>
                </Row>
              ))
            }
          </> :
          <>
            <Empty description={`${titleEditModal} chưa có nhân viên nào`}></Empty>
          </>
        }
      </Modal>
    </>
  )
}

// Index.Layout = AdminLayout

export default (Index)

