import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import React, { useEffect, useState, Component } from 'react'
import {
  Row,
  Col,
  Image,
  Menu,
  Button,
  Table,
  Space,
  Modal,
  message,
  Input, 
  Form,
  Layout,
  Icon
} from 'antd'
import {
  ContainerOutlined,
  DesktopOutlined,
  LeftOutlined,
  QuestionCircleTwoTone,
  RightOutlined,
  SafetyOutlined,
  UserOutlined
} from '@ant-design/icons'

import { useSelector, useDispatch } from 'react-redux'
import { createDepartment, listDepartment } from '../../actions/department'
import { useHistory } from "react-router"

import Dashboard from './Sidebar'
import Meseros from '../login';

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

class RouterApp extends Component {

    state = {
        collapsed: false,
    };

    onCollapse = (collapsed) => {
        this.setState({ collapsed });
    }
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    render() {
        return (
            <Router>
                <Layout style={{ minHeight: '100vh' }}>

                    <Sider
                        collapsible
                        collapsed={this.state.collapsed}
                        onCollapse={this.onCollapse}>
                        <div className="logo" />
                        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                            <Menu.Item key="1">
                                {/* <Icon type="pie-chart" /> */}
                                <span>Deshboard</span>
                                <Link to="/" />
                            </Menu.Item>
                            <Menu.Item key="2">
                                {/* <Icon type="desktop" /> */}
                                <span>Meseros</span>
                                <Link to="/meseros" />
                            </Menu.Item>
                        </Menu>
                    </Sider>

                    
                    <Layout>
                        <Header style={{ background: '#fff', padding: 0, paddingLeft: 16 }}>
                            {/* <Icon */}
                                {/* className="trigger"
                                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                style={{ cursor: 'pointer' }}
                                onClick={this.toggle} */}
                            {/* /> */}
                        </Header>
                        <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
                            <Route exact path="/" component={Dashboard} />
                            <Route path="/login" component={Meseros} />
                        </Content>

                    </Layout>

                </Layout>
            </Router>
        );
    }
}


export default RouterApp;