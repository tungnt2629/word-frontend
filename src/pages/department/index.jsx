import React, { useState, useEffect } from 'react'
import {
  BackTop,
  Row,
  Col,
  Image,
  Dropdown,
  Menu,
  Button,
  Card,
  Avatar,
  Modal,
  Tooltip,
  Input, 
  Space,
  Empty,
  Spin
} from 'antd'
import {
  FileTextTwoTone,
  QuestionOutlined,
  ArrowUpOutlined,
  PlusCircleOutlined,
  SearchOutlined,
  InfoCircleOutlined,
  LogoutOutlined,
  TeamOutlined,
  DeleteOutlined,
  PlusOutlined
} from '@ant-design/icons'
import Meta from 'antd/lib/card/Meta'
import { useSelector, useDispatch } from 'react-redux'
import { viewProfile } from '../../actions/auth'
import { createDocument, setListComment, setListCommentAndUserComments, setListUserComment, showDocument } from '../../actions/document'
// import CheckAuth from '../middleware/auth'
import { useHistory } from "react-router-dom"
import ApiService from "../../services/index"
import { useParams } from "react-router-dom"
import { listDepartment, setListTemplate, showDepartment } from '../../actions/department'
import logo from '../../images/logo.png'

function Department() {
  const history = useHistory()
  const dispatch = useDispatch()
  const [listView, setListView] = useState(false)
  const [visible, setVisible] = useState(false)
  const [loadingState, setLoadingState] = useState(true)

  const auth = useSelector(state => state.auth)
  // const document = useSelector(state => state.document)
  const department = useSelector(state => state.department)

  const { id: departmentId } = useParams()

  useEffect(() => {
    dispatch(viewProfile(history))
    // dispatch(listDocument(history))
    dispatch(listDepartment(history))
    dispatch(setListComment([]))
    dispatch(setListTemplate([]))
    dispatch(setListUserComment([]))
    dispatch(setListCommentAndUserComments([]))
  }, [])

  useEffect(() => {
    if(departmentId) {
      dispatch(showDepartment(departmentId, history))
    }
  }, [])

  // useEffect(() => {
  //   if(department.listTemplates.length > 0) {
  //     setLoadingState(false)
  //   }
  // }, [department])

  // console.log("currentDepartment", department)
  return (
    <Spin tip="Đang tải ..." spinning={false} size="large">
      {
        auth.profile &&
        <div className="page department">
          <div className="header-wrapper">
            <Row className='header' justify='space-between' align='center'>
              <Col className='col-left'>
                <Image
                  style={{ paddingLeft: 15, paddingRight: 15, width: 80 }}
                  className='header-icon'
                  onClick={
                    () => {
                      history.push('/')
                    }
                  }
                  src={logo}
                />
                <h3>Lisod VN</h3>
              </Col>
              <Col className='col-right'>
                <Tooltip placement="top" title={'Cài đặt'}>
                  <QuestionOutlined style= {{ paddingLeft:15, paddingRight:15 }}/>
                </Tooltip>
                <Dropdown overlay={
                  <Menu>
                    <Menu.Item danger onClick={() => {
                      ApiService.deleteAllCookies()
                      history.push("/login")
                    }}>
                      <Space>
                        <LogoutOutlined />
                        Đăng xuất
                      </Space>
                    </Menu.Item>
                    <Menu.Item onClick={() => {
                      history.push("/about")
                    }}>
                      <Space>
                        <InfoCircleOutlined />
                        Thông tin cá nhân
                      </Space>
                    </Menu.Item>
                    {
                      auth.profile.role !== null && auth.profile.role === 1 &&
                      <Menu.Item onClick={() => {
                        history.push("/admin")
                      }}>
                        <Space>
                          <InfoCircleOutlined />
                          Trang quản trị
                        </Space>
                      </Menu.Item>
                    }
                  </Menu>
                }>
                  <Image
                    alt="avatar"
                    className="avatar"
                    src={auth.profile.avatar}
                  />
                </Dropdown>
              </Col>
            </Row>
          </div>
          {/* END HEADER */}
          {
            department &&
            <div className="template-wrapper">
              <div className="content">
                <Row justify="space-between" className="title-wrapper">
                </Row>

                <Row className="template-detail" justify="space-between" gutter={[0, 10]}>
                  <Col span={24} className="gutter-row">
                    <Row>
                      <TeamOutlined className="banner-icon"/>
                    </Row>
                    <Row><div className="banner-text">Danh sách tài liệu mẫu {department.currentDepartment}</div></Row>
                  </Col>
                </Row>
              </div>
            </div>
          }

          <Row justify="center">
            <Row justify='space-between' className="content-title">
              <Col>
                {/* <Button>
                  create new 
                </Button> */}
              </Col>
              <Col>
                {/* <UnorderedListOutlined className="right-icon" onClick={()=>{
                  setListView(false)
                }}/> */}
                
              </Col>
            </Row>
          </Row>

          <div className="content-wrapper">
            <Row className="main-document">
              <Col span={6} className="item">
                <Card
                  className = "new-empty-document"
                  actions={[
                  ]}
                  onClick={
                    () => {
                      dispatch(createDocument(
                        {
                          title: 'Tài liệu không có tiêu đề',
                          content: '',
                          department_id: departmentId
                        },
                        history)) 
                    // console.log('abc')                                 
                    }
                  }
                >
                    <Space direction="vertical">
                      <Tooltip key="edit" placement="top" className="create-by-template-button" title={'Tạo mới tài liệu không theo template'}>
                        <PlusCircleOutlined
                          className="new-empty-document-icon"
                        />
                      </Tooltip>
                    </Space>
                </Card>
              </Col>
              {
                department.listTemplates && department.listTemplates.map((item, key) => (
                  <React.Fragment key={item}>
                    <Col span={6} className="item">
                      <Card
                        cover={
                          <Image
                            alt="example"
                            src={logo}
                          />
                        }
                        actions={[
                          // <Tooltip key="setting" placement="top" title={'Cài đặt'}>
                          //   <SettingOutlined/>
                          // </Tooltip>,
                          <Tooltip key="edit" placement="top" className="create-by-template-button" title={'Tạo mới tài liệu theo template này'}>
                            <PlusOutlined
                              onClick={
                                () => {
                                  dispatch(createDocument(
                                    {
                                      title: item.title,
                                      content: item.content,
                                      department_id: departmentId
                                    },
                                    history))                                  
                                }
                              }
                            />
                          </Tooltip>
                        ]}
                        className='document-card-global'
                      >
                        <Meta
                          avatar={<Avatar src="https://ui-avatars.com/api/?name=John+Doe" />}
                          title={item.title}
                          description={
                            'Nội dung: ' + item.content.replace(/<[^>]+>/g, '').slice(0,50)
                          }
                          // onClick={() => {
                          //   dispatch(showDocument(item.id, history))
                          //   history.push( `/documents/${item.id}`)
                          // }}
                        />
                      </Card>
                    </Col>
                  </React.Fragment>
                ))
              }
            </Row>
          </div>

          <BackTop>
            <div><ArrowUpOutlined/></div>
          </BackTop>

          <Modal
            title="Danh sách tài liệu mẫu"
            centered
            visible={visible}
            onOk={() => setVisible(false)}
            onCancel={() => setVisible(false)}
            width={1000}
            bodyStyle={{ overflowX: 'scroll' }}
          >
              <Row className="template-detail" justify="space-between" gutter={[0, 10]}>
                {
                  department.listDepartments && department.listDepartments.map((item, key) => (
                    <React.Fragment key={key+1}>
                      <Col span={4} className="gutter-row department-list" onClick={() => {
                          history.push('/department/'+ item.id)
                        }}>
                        {item.name}
                        <PlusCircleOutlined className="new-document" />
                      </Col>
                    </React.Fragment>
                  )) 
                }
              </Row>
          </Modal>
        </div>
      } 
    </Spin>
  )
}

export default Department