// import React, { useState, useEffect } from 'react'
// import {
//   BackTop,
//   Row,
//   Col,
//   Image,
//   Dropdown,
//   Menu,
//   Button,
//   Card,
//   Avatar,
//   Modal,
//   Tooltip,
//   Input,
//   Space,
//   Empty,
//   message,
//   Divider
// } from 'antd'
// import {
//   FileTextTwoTone,
//   QuestionOutlined,
//   ArrowUpOutlined,
//   PlusCircleOutlined,
//   FolderTwoTone,
//   UnorderedListOutlined,
//   SortAscendingOutlined,
//   SettingOutlined,
//   EditOutlined,
//   InfoCircleOutlined,
//   LogoutOutlined,
//   DeleteOutlined,
//   GoldTwoTone,
//   ShareAltOutlined
// } from '@ant-design/icons'
// import Meta from 'antd/lib/card/Meta'
// import { useSelector, useDispatch } from 'react-redux'
// import { viewProfile } from '../actions/auth'
// import { deleteDocument, listDocument, listShare, setListComment, setListUserComment, showDocument, showSharePerson } from '../actions/document'
// import { listDepartment, showDepartment } from '../actions/department'
// // import CheckAuth from '../middleware/auth'
// import { useHistory } from "react-router-dom"
// import ApiService from "../services/index"

// function Home() {
//   const history = useHistory()
//   const dispatch = useDispatch()
//   const auth = useSelector(state => state.auth)
//   const document = useSelector(state => state.document)

//   useEffect(() => {
//     dispatch(viewProfile(history))
//     dispatch(listDocument(history))
//     dispatch(listDepartment(history))
//   }, [])

//   useEffect(() => {
//     if (auth.profile) {
//       dispatch(listShare(history, auth.profile.id))
//       dispatch(showSharePerson(history, auth.profile.id))
//     }
//   }, [auth])

//   const avatarMenu = (
//     <Menu>
//       <Menu.Item key='1' danger onClick={() => {
//         ApiService.deleteAllCookies()
//         history.push("/login")
//       }}>
//         <Space>
//           <LogoutOutlined />
//           Đăng xuất
//         </Space>
//       </Menu.Item>
//       <Menu.Item key='2' onClick={() => {
//         history.push("/about")
//       }}>
//         <Space>
//           <InfoCircleOutlined />
//           Thông tin cá nhân
//         </Space>
//       </Menu.Item>
//       {
//         auth.profile && auth.profile.role === 1 &&
//         <Menu.Item key='3' onClick={() => {
//           history.push("/admin")
//         }}>
//           <Space>
//             <InfoCircleOutlined />
//             Trang quản trị
//           </Space>
//         </Menu.Item>
//       }
//     </Menu>
//   )
//   useEffect(() => {
//     if(document.listDocuments.length !== 0 && auth){
//       var array = document.listDocuments,
//       result = array
//         .filter(v => v.user_id === auth.profile.id)
//         .map(v => v);
//       document.listDocuments = result
//     }
//   }, [auth, document.listDocuments])

//   const { Search } = Input

//   const onSearch = value => {
//     // console.log(value)
//     if (!value || value === '') {
//       message.warning("Bạn vui lòng nhập tên tài liệu cần tìm kiếm")
//     } else {
//       dispatch(listDocument(history, value))
//     }
//   }

//   return (
//     <>
//       {
//         auth.profile &&
//         <div className="page home">
//           <div className="header-wrapper">
//             <Row className='header'>
//               <Col className='col-left' span={4}>
//                 {/* <Tooltip placement="top" title={'Cài đặt'}>
//                   <SettingFilled style={{ fontSize:20 }}/>
//                 </Tooltip> */}
//                 <FileTextTwoTone
//                   style={{ paddingLeft: 15, paddingRight: 15 }}
//                   className='header-icon'
//                   onClick={
//                     () => {
//                       // dispatch(setListComment([]))
//                       // dispatch(setListUserComment([]))
//                       history.push('/')
//                     }
//                   }
//                 />
//                 <h3>Tài liệu tổng hợp</h3>
//                 {/* <h3>{auth.profile.avatar}</h3> */}
//               </Col>
//               <Col span={3}></Col>
//               <Col className='col-center' span={10}>
//                 <Search placeholder="Tìm kiếm tài liệu" onSearch={onSearch} enterButton className="search-bar" />
//               </Col>
//               <Col span={3}></Col>
//               <Col className='col-right' span={4} >
//                 <Tooltip color='blue' placement="top" title={'Thông tin và trợ giúp'}>
//                   <QuestionOutlined 
//                     style={{ paddingLeft: 15, paddingRight: 15 }} 
//                     onClick={()=> {
//                       history.push('/faq')
//                     }}  
//                   />
//                 </Tooltip>
//                 <Dropdown overlay={avatarMenu}>
//                   {
//                     auth.profile.avatar ?
//                       <Image
//                         alt="avatar"
//                         className="avatar"
//                         src={auth.profile.avatar}
//                       />
//                       :
//                       <Image
//                         alt="avatar"
//                         className="avatar"
//                         src={'https://ui-avatars.com/api/?background=random'}
//                       />
//                   }
//                 </Dropdown>
//               </Col>
//             </Row>
//           </div>

//         </div>
//       }
//     </>
//   )
// }

// // export default CheckAuth(Home)
// export default Home