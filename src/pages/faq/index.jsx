import React, { useState, useEffect } from 'react'
import {
  BackTop,
  Row,
  Col,
  Image,
  Dropdown,
  Menu,
  Tooltip,
  Space,
  Divider
} from 'antd'
import {
  QuestionOutlined,
  ArrowUpOutlined,
  InfoCircleOutlined,
  LogoutOutlined,
  MessageOutlined,
  QuestionCircleTwoTone
} from '@ant-design/icons'
import { useSelector, useDispatch } from 'react-redux'
import { viewProfile } from '../../actions/auth'
import { useHistory } from "react-router-dom"
import ApiService from "../../services/index"
import logo from '../../images/logo.png'

function Faq() {
  const history = useHistory()
  const dispatch = useDispatch()
  const auth = useSelector(state => state.auth)

  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  const dataSource = [
    {
      title: 'Cách thêm mới tài liệu ? ',
      content: 'Trả lời: Bạn truy cập vào trang đăng nhập, tiến hành đăng nhập, lựa chọn phòng ban muốn thêm tài liệu. Ở đây bạn sẽ có 2 lựa chọn: thêm mới tài liệu sử dụng tài liệu mẫu hoặc không sử dụng tài liệu mẫu.'
    },
    {
      title: 'Cách truy cập trang quản trị của quản trị viên ?',
      content: 'Trả lời: Bạn tiến hành đăng nhập tài khoản quản trị viên, sau dó đưa chuột vào vị trí hình avatar của bạn, một của sổ mở ra và bạn cần lựa chọn vào mục "Trang quản trị"'
    },
    {
      title: 'Không thể thêm mới được tài liệu sử dụng tài liệu mẫu ?',
      content: 'Trả lời: Bạn vui lòng kiểm tra lại đường truyền mạng, hoặc liên hệ CSKH 0965327848 để thực hiện yêu cầu CSKH'
    },
    {
      title: 'Làm sao để đăng ký tài khoản ?',
      content: `Trả lời : Bạn vui lòng truy cập trang đăng ký tài khoản để thực hiện thêm mới tài khoản cá nhân`
    },
    {
      title: 'Có thể xóa tài khoản cá nhân hay không ?',
      content: 'Trả lời: Bạn chỉ có thể xóa tài khoản cá nhân khi sử dụng tài khoản quản trị viên'
    },
    {
      title: 'Tài liệu có thể chia sẻ tới tất cả mọi người hay không ?',
      content: 'Trả lời: Có thể chia sẻ tài liệu cho tất cả mọi người bằng cách chọn chức năng chia sẻ tới tất cả mợi người'
    },
    {
      title: 'Hệ thống có hỗ trợ chỉnh sửa theo thời gian thực hay không ?',
      content: 'Trả lời: Có thể, khi 2 tài khoản được chia sẻ tài liệu với nhau cùng chỉnh sửa một tài liệu, hoặc khi 1 cá nhân dùng nhiều tab trình duyệt hoặc nhiều trình duyệt đăng nhập cùng tài khoản, chỉnh sửa cùng một tài liệu thì bạn sẽ thấy tài liệu khi chỉnh sửa sẽ được cập nhật theo thời gian thực, đó chính là điểm mạnh của hệ thống'
    }
  ]
  return (
    <React.Fragment>
      {
        auth.profile &&
        <div className="page home">
          <div className="header-wrapper">
            <Row className='header'>
              <Col className='col-left' span={4}>
                {/* <Tooltip placement="top" title={'Cài đặt'}>
                  <SettingFilled style={{ fontSize:20 }}/>
                </Tooltip> */}
                <Image
                  style={{ paddingLeft: 15, paddingRight: 15, width: 80 }}
                  src={logo}
                  // className='header-icon'
                  onClick={
                    () => {
                      history.push('/')
                    }
                  }
              />
                <h3>Lisod VN</h3>
                {/* <h3>{auth.profile.avatar}</h3> */}
              </Col>
              <Col span={3}></Col>
              <Col className='col-center' span={10}>
                {/* <Search placeholder="Tìm kiếm tài liệu" onSearch={onSearch} enterButton className="search-bar"/> */}
              </Col>
              <Col span={3}></Col>
              <Col className='col-right' span={4} >
                <Tooltip placement="top" color={'blue'} title={'Thông tin và trợ giúp'}>
                  <QuestionOutlined style= {{ paddingLeft:15, paddingRight:15 }} onClick={() => {
                      history.push('/faq')
                  }}/>
                </Tooltip>
                <Dropdown overlay={
                  <Menu>
                    <Menu.Item danger onClick={() => {
                      ApiService.deleteAllCookies()
                      history.push("/login")
                    }}>
                      <Space>
                        <LogoutOutlined />
                        Đăng xuất
                      </Space>
                    </Menu.Item>
                    <Menu.Item onClick={() => {
                      history.push("/about")
                    }}>
                      <Space>
                        <InfoCircleOutlined />
                        Thông tin cá nhân
                      </Space>
                    </Menu.Item>
                    {
                      auth.profile.role !== null && auth.profile.role === 1 &&
                      <Menu.Item onClick={() => {
                        history.push("/admin")
                      }}>
                        <Space>
                          <InfoCircleOutlined />
                          Trang quản trị
                        </Space>
                      </Menu.Item>
                    }
                  </Menu>
                }>
                  {
                    auth.profile.avatar ?
                    <Image
                      alt="avatar"
                      className="avatar"
                      src={auth.profile.avatar}
                    />
                    :
                    <Image
                      alt="avatar"
                      className="avatar"
                      src={'https://ui-avatars.com/api/?background=random'}
                    />
                  }
                </Dropdown>
              </Col>
            </Row>
          </div>

          <div className="template-wrapper">
            <div className="content">
              <Row justify="space-between" className="title-wrapper">
                <Col span={12} className="new-document">
                  <QuestionCircleTwoTone /> Những câu hỏi thường gặp 
                </Col>
                <Col span={12} className="template-list">
                </Col>
              </Row>
            </div>
          </div>

          <Row justify="center">
            <Row justify='space-between' className="content-title">
              <div className='faq content'>
                
                {
                  dataSource.map((item, key) => (
                    <>
                      <Row>{item.title}</Row>
                      <Row key={key+1} className='faq reply-section'>
                        <Space>
                          <MessageOutlined />
                          <Divider type='vertical'/>
                          {item.content}
                        </Space>
                      </Row>
                      <Divider/>
                    </>
                  ))
                }
              </div>
            </Row>
          </Row>

          <BackTop>
            <div><ArrowUpOutlined/></div>
          </BackTop>
          
        </div>
      } 
    </React.Fragment>
  )
}

export default Faq