import React from 'react'
import { Result, Button } from 'antd'
import { useHistory } from 'react-router'

function Notfound() {
  const history = useHistory()

  return (
    <div>
      <Result
        status="404"
        title="404"
        subTitle="Rất tiếc, Trang này không tồn tại."
        extra={<Button type="primary" onClick={() => {
          history.push('/')
        }}>Về trang chủ</Button>}
      />
    </div>
  )
}

export default Notfound
