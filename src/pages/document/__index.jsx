
import React, { useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import dynamic from 'next/dynamic'
import 'react-quill/dist/quill.snow.css'
import CheckAuth from '../../middleware/auth'
// import { useRouter } from 'next/router'
import { io } from 'socket.io-client'
// const ReactQuill = dynamic(() => import('react-quill'), { ssr: false })
import { Editor } from '@tinymce/tinymce-react';
import { Button, Row, Col, Space, Image, Menu, Dropdown, Input } from 'antd'
import {
  ShareAltOutlined,
  FileWordTwoTone,
  StarOutlined,
  MessageOutlined,
  SaveOutlined,
  CloudUploadOutlined
} from '@ant-design/icons'
import { showDocument, setUpdateDocument, setNewDocument } from '../../redux/actions/document'
import ApiService from '../services'

const SAVE_INTERVAL_MS = 20000
function TextEditor({initialValue}) {
  const dispatch = useDispatch()
  const document = useSelector(state => state.document)
  // const router = useRouter()
  const documentId = ApiService.getQueryVariable('docId')
  const editorRef = useRef(null)
  const [socket, setSocket] = useState()
  const [value, setValue] = useState(initialValue ?? '')
  const [currentDocument, setCurrentDocument] = useState(null)
  const [receivedData, setReceivedData] = useState(null)

  // console.log('editor Ref ==> ', editorRef)
  useEffect(() => {
    setValue(initialValue ?? '')
  }, [initialValue])

  useEffect(() => {
    async function fetchData() {
      if (documentId) {
        dispatch(showDocument(documentId))
      }
      // else router.push('/')
    }
    fetchData()
  }, [])

  useEffect(() => {
    const socketIO = io.connect('http://localhost:6009')
    setSocket(socketIO)

    return (() => {
      socket.disconnect()
    })
  }, [])

  // set default data to document
  useEffect(() => {
    async function fetchData() {
      // console.log('------> setValue from store ---------> ')
      if (document.currentDocument && document.currentDocument.content) {
        setValue(document.currentDocument.content)
      }
    }
    fetchData()
  }, [document.currentDocument])

  // useEffect(() => {
  //   async function fetchData() {
  //     if (documentId) {
  //       dispatch(showDocument(documentId))
  //     }
  //     else router.push('/')
  //   }
  //   fetchData()
  // }, [])

  // save document
  useEffect(() => {
    // console.log('socket and value checking null or not ', socket, 'value:', value)
    if (socket == null || value == null || document.currentDocument == null || value == '') return

    const interval = setInterval(() => {
      let newDocument = {
        documentId: documentId,
        title: 'Tài liệu không có tiêu đề',
        content: value
      }
      // console.log('............saving from client .............')

      if (document.currentDocument) {
        ApiService.post('/documents/update/'+ documentId, newDocument)
          .then((response) => {
            if (response.status === 200) {
              dispatch(setUpdateDocument(newDocument))
            }
          })
      }
    }, SAVE_INTERVAL_MS)

    return () => {
      clearInterval(interval)
    }
  }, [socket, document.currentDocument, value])

  useEffect(() => {
    if (socket) {
      socket.on('receive-data', (received) => {
        // console.log('received data - OK bro:', received.content)
        setValue(received.content)
      })
      return (() => {
        socket.off('receive-data')
      })
    }
  }, [socket])

  const handleContentChange = (e) => {
    setValue(e)
    let newDocument = {
      documentId: documentId,
      title: 'tài liệu không có tiêu đề',
      content: e
    }
    if (socket && newDocument) {
      socket.emit('update-document', newDocument)
      // console.log('emit send content')
      // setValue(currentDocument.content)
    }
  }

  // console.log('real value state:', value)
  return (
    <>
      {
        document.currentDocument &&
        <>
          <div className="header">
            <Row gutter={40} className="navbar">
              <Col span={6} xs={24} md={15} lg={19} xl={20}>
                <Space >
                  <FileWordTwoTone className="logo" />

                  <div className="title-wrapper">
                    <Space className="title-space">
                      <Input
                        // value={document.currentDocument.title}
                        value={'123'}
                        className="document-title"
                        // onChange={e => handleTitleChange(e)}
                      ></Input>
                      <StarOutlined />
                      <SaveOutlined />
                      <CloudUploadOutlined />
                    </Space>
                    <h4 className="document-subtitle">
                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">File</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Edit</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">View</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Insert</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Format</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Tool</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Add on</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Help</Button>
                      </Dropdown>
                    </h4>
                  </div>
                </Space>
              </Col>
              <Col span={6} xs={24} md={9} lg={5} xl={4} className="avatar-wrapper">
                {/* <Space> */}
                <Dropdown overlay={menu} placement="bottomRight">
                  <MessageOutlined />
                </Dropdown>

                <Dropdown overlay={menu} placement="bottomRight">
                  <Button type="" icon={<ShareAltOutlined/>}>Share</Button>
                </Dropdown>
                <Image
                  className="avatar"
                  src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                  alt=""
                />
                {/* </Space> */}
              </Col>
            </Row>
          </div>
          {
            document.currentDocument.content &&
            <>
              <Editor
                onInit={(evt, editor) => editorRef.current = editor}
                initialValue={initialValue}
                value={value}
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                  ],
                  toolbar: 'undo redo | formatselect | ' +
                  'bold italic backcolor | alignleft aligncenter ' +
                  'alignright alignjustify | bullist numlist outdent indent | ' +
                  'removeformat | help',
                  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                }}
                onEditorChange={(e) => { handleContentChange(e) }}
                // onEditorChange={e => handleContentChange(e)}
                ref={editorRef}
              />
            </>
          }
        </>
      }
    </>
  )
}

const menu = (
  <Menu>
    <Menu.Item key={1}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        1st menu item
      </a>
    </Menu.Item>
    <Menu.Item key={2}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        2nd menu item
      </a>
    </Menu.Item>
    <Menu.Item key={3}>
      <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
        3rd menu item
      </a>
    </Menu.Item>
  </Menu>
)

export default CheckAuth(TextEditor)
