import React, { useCallback, useEffect, useState } from "react"
import { Button, Row, Col, Space, Image, Menu, Dropdown, Input } from 'antd'
import {
  ShareAltOutlined,
  FileWordTwoTone,
  StarOutlined,
  MessageOutlined,
  SaveOutlined,
  CloudUploadOutlined
} from '@ant-design/icons'
import Quill from "quill"
import "quill/dist/quill.snow.css"
import { io } from "socket.io-client"
import { useParams } from "react-router-dom"
import { showDocument } from "../../actions/document"
import { useHistory } from "react-router"
import ApiService from "../../services"

const SAVE_INTERVAL_MS = 2000
const TOOLBAR_OPTIONS = [
  [{ header: [1, 2, 3, 4, 5, 6, false] }],
  [{ font: [] }],
  [{ list: "ordered" }, { list: "bullet" }],
  ["bold", "italic", "underline"],
  [{ color: [] }, { background: [] }],
  [{ script: "sub" }, { script: "super" }],
  [{ align: [] }],
  ["image", "blockquote", "code-block"],
  ["clean"],
]

export default function TextEditor() {
  const { id: documentId } = useParams()
  const [socket, setSocket] = useState()
  const [quill, setQuill] = useState()
  const history = useHistory()

  useEffect(() => {
    const s = io("http://localhost:3001")
    setSocket(s)

    return () => {
      s.disconnect()
    }
  }, [])

  const showCurrentDocument = (id) => {
    ApiService.get('/documents/show/'+ id)
      .then((response) => {
        // if (response.data.status === 200) {
        //   dispatch(setUpdateDocument(response.data.data))
        // }
        // else message.error('error')
        // console.log("response get document: ", response)
        return response
      })
      .catch((errore) => {
      })
  }

  useEffect(() => {
    if (socket == null || quill == null) return

    socket.once("load-document", document => {
      quill.setContents(document)
      quill.enable()
    })
    async function fetchMyAPI() {
      // console.log("current doc ID", documentId)
      const response = await showCurrentDocument(documentId)
      // console.log("show doc response,", response)
      if (response.data.status === 200) {
        socket.emit("get-document", response)
      }
    }
    
    fetchMyAPI()
    
  }, [socket, quill, documentId])

  useEffect(() => {
    if (socket == null || quill == null) return

    const interval = setInterval(() => {
      socket.emit("save-document", quill.getContents())
    }, SAVE_INTERVAL_MS)

    return () => {
      clearInterval(interval)
    }
  }, [socket, quill])

  useEffect(() => {
    if (socket == null || quill == null) return

    const handler = delta => {
      quill.updateContents(delta)
    }
    socket.on("receive-changes", handler)

    return () => {
      socket.off("receive-changes", handler)
    }
  }, [socket, quill])

  useEffect(() => {
    if (socket == null || quill == null) return

    const handler = (delta, oldDelta, source) => {
      if (source !== "user") return
      socket.emit("send-changes", delta)
    }
    quill.on("text-change", handler)

    return () => {
      quill.off("text-change", handler)
    }
  }, [socket, quill])

  const wrapperRef = useCallback(wrapper => {
    if (wrapper == null) return

    wrapper.innerHTML = ""
    const editor = document.createElement("div")
    wrapper.append(editor)
    const q = new Quill(editor, {
      theme: "snow",
      modules: { toolbar: TOOLBAR_OPTIONS },
    })
    q.disable()
    q.setText("Loading...")
    setQuill(q)
  }, [])

  const menu = (
    <Menu>
      <Menu.Item key={1}>
        <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
          1st menu item
        </a>
      </Menu.Item>
      <Menu.Item key={2}>
        <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
          2nd menu item
        </a>
      </Menu.Item>
      <Menu.Item key={3}>
        <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
          3rd menu item
        </a>
      </Menu.Item>
    </Menu>
  )
  return (
    <React.Fragment>
      <div className="header">
            <Row gutter={40} className="navbar">
              <Col span={6} xs={24} md={15} lg={19} xl={21}>
                <Space >
                  <FileWordTwoTone className="logo" />

                  <div className="title-wrapper">
                    <Space className="title-space">
                      <Input
                        // value={document.currentDocument.title}
                        value={'123'}
                        className="document-title"
                        // onChange={e => handleTitleChange(e)}
                      ></Input>
                      <StarOutlined />
                      <SaveOutlined />
                      <CloudUploadOutlined />
                    </Space>
                    <h4 className="document-subtitle">
                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">File</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Edit</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">View</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Insert</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Format</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Tool</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Add on</Button>
                      </Dropdown>

                      <Dropdown overlay={menu} placement="bottomLeft" className="subtitle-menu">
                        <Button className="tool">Help</Button>
                      </Dropdown>
                    </h4>
                  </div>
                </Space>
              </Col>
              <Col span={6} xs={24} md={9} lg={5} xl={3} className="avatar-wrapper">
                {/* <Space> */}
                <Dropdown overlay={menu} placement="bottomRight">
                  <MessageOutlined />
                </Dropdown>

                <Dropdown overlay={menu} placement="bottomRight">
                  <Button type="" icon={<ShareAltOutlined/>}>Share</Button>
                </Dropdown>
                <Image
                  className="avatar"
                  src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                  alt=""
                />
                {/* </Space> */}
              </Col>
            </Row>
          </div>
      <div className="container" ref={wrapperRef}></div>
      
    </React.Fragment>
  )
}
