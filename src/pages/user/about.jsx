import React, { useEffect, useState } from 'react'
// import { useRouter } from 'next/router'
import { viewProfile } from '../../actions/auth'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from "react-router-dom"
// import Meta from 'antd/lib/card/Meta'
import {
  BackTop,
  Row,
  Descriptions, Badge,
  Col,
  Image,
  Modal,
  Tooltip,
  Input,
  Space,
  Dropdown,
  Menu,
  Button,
  Alert,
  DatePicker,
  Select,
  Form
} from 'antd'
import {
  EditOutlined,
  QuestionOutlined,
  LogoutOutlined,
  InfoCircleOutlined,
  UserOutlined,
  PhoneOutlined,
  MailOutlined,
  LockOutlined
} from '@ant-design/icons'
// import { showDocument } from '../../actions/document'
import ApiService from '../../services'
import moment from 'moment'
import logo from '../../images/logo.png'
import { updateUser } from '../../actions/user'
import { listDepartment } from '../../actions/department'

function About() {
  const { Option } = Select;
  const history = useHistory()
  const dispatch = useDispatch()
  // const document = useSelector(state => state.document)
  const auth = useSelector(state => state.auth)
  const department = useSelector(state => state.department)

  const [editModal, setEditModal] = useState()

  useEffect(() => {
    dispatch(viewProfile(history))
  }, [])

  useEffect(() => {
    async function fetchData() {
      dispatch(listDepartment(history))
    }
    fetchData()
  }, [])

  function disabledDate(current) {
    // Can not select days before today and today
    return current && current > moment().endOf('day');
  }

  console.log("AUTH", auth)
  return (
    <div className='home'>
    <div className="header-wrapper">
          <Row className='header' justify='space-between' align='middle'>
            <Col className='col-left'>
              {/* <Tooltip placement="top" title={'Cài đặt'}>
                <SettingFilled style={{ fontSize:20 }}/>
              </Tooltip> */}
              <Image
                style={{ paddingLeft: 15, paddingRight: 15, width: 80 }}
                src={logo}
                // className='header-icon'
                onClick={
                  () => {
                    history.push('/')
                  }
                }
              />
              <h3>Lisod VN</h3>
            </Col>
            <Col></Col>
            <Col></Col>
            <Col className='col-right' span={4} >
              <Tooltip color='blue' placement="top" title={'Thông tin và trợ giúp'}>
                <QuestionOutlined
                  style={{ paddingLeft: 15, paddingRight: 15 }} 
                  onClick={()=> {
                    history.push('/faq')
                  }}  
                />
              </Tooltip>
              <Dropdown overlay={
                  <Menu>
                  <Menu.Item key='1' danger onClick={() => {
                    ApiService.deleteAllCookies()
                    history.push("/login")
                  }}>
                    <Space>
                      <LogoutOutlined />
                      Đăng xuất
                    </Space>
                  </Menu.Item>
                  <Menu.Item key='2' onClick={() => {
                    history.push("/about")
                  }}>
                    <Space>
                      <InfoCircleOutlined />
                      Thông tin cá nhân
                    </Space>
                  </Menu.Item>
                  {
                    auth.profile && auth.profile.role === 1 &&
                    <Menu.Item key='3' onClick={() => {
                      history.push("/admin")
                    }}>
                      <Space>
                        <InfoCircleOutlined />
                        Trang quản trị
                      </Space>
                    </Menu.Item>
                  }
                </Menu>
              }>
                {
                  auth.profile ?
                    <Image
                      alt="avatar"
                      className="avatar"
                      src={auth.profile.avatar}
                    />
                    :
                    <Image
                      alt="avatar"
                      className="avatar"
                      src={'https://ui-avatars.com/api/?background=random'}
                    />
                }
                
              </Dropdown>
            </Col>
          </Row>
        </div>
    {
      auth.profile &&
      <div className="about">
        
        <div className="about-content">
          <Row gutter={[16, 16]} justify={'center'}>
            <Col md={6} xs={24} className="avatar-wrapper">
              <div className="avatar">
                <Image
                  width={200}
                  src={auth.profile.avatar}
                  className="avatar-img"
                />
                <Button onClick={()=> {
                  setEditModal(true)
                  
                  }}
                  type='primary'
                  className='edit-user-button'
                  >Sửa thông tin cá nhân<EditOutlined />
                </Button>
              </div>
            </Col>
            <Col md={15} xs={24}>
              <Descriptions title={
                <Space>
                  Thông tin cá nhân
                  <Modal
                    style={{top:20}}
                    visible={editModal}
                    title={`Chỉnh sửa người dùng: ${auth.profile.name} (ID: ${auth.profile.id})`}
                    footer={[
                    ]}
                    onOk={()=>{setEditModal(false)}}
                    onCancel={()=>{setEditModal(false)}}
                  >
                    <div className="form">
                      {
                        auth.error &&
                        <Alert
                          message={auth.error}
                          type="error"
                          showIcon
                        />
                      }
                      {
                        auth.profile &&
                        <Form
                          name="basic"
                          onFinish={(values) => {
                            values['dob'] = (moment(values.dob).format('DD/MM/YYYY h:mm'))
                            dispatch(updateUser(values, auth.profile.id, history))
                          }}
                          onOk={()=>{setEditModal(false)}}
                          autoComplete="off"
                          initialValues={{
                            name: auth.profile.name,
                            gender: auth.profile.gender,
                            dob: moment(auth.profile.dob, 'DD/MM/YYYY hh:mm'),
                            phone: auth.profile.phone,
                            email: auth.profile.email,
                            // department_id: recordEdit.department.name
                            // ["password"]: auth.profile.password,
                          }}
                        >
                          <span>Họ tên</span>
                          <Form.Item
                            name="name"
                            rules={[
                              {
                                required: true,
                                message: 'Họ tên không được để trống'
                              }
                            ]}
                          >
                            <Input addonAfter={<UserOutlined />} />
                          </Form.Item>

                          <span>Giới tính</span>
                          <Form.Item
                            name="gender"
                            rules={[
                              {
                                required: true,
                                message: 'Giới tính không được để trống'
                              }
                            ]}
                          >
                            <Select style={{ width: '100%' }}>
                              <Option value="nam">Nam</Option>
                              <Option value="nu">Nữ</Option>
                            </Select>
                          </Form.Item>

                          <span>Thuộc phòng ban</span>
                          <Form.Item
                            name="department_id"
                            rules={[
                              {
                                required: true,
                                message: 'Bạn cần lựa chọn phòng ban'
                              }
                            ]}
                          >
                            <Select defaultValue="" style={{ width: '100%' }}>
                              {
                                department.listDepartments.length !== 0 &&
                                <>
                                  {
                                    department.listDepartments.map((item, key) => (
                                      <Option key={key+1} value={item.id}>{item.name}</Option>
                                    ))
                                  }
                                </>
                              }
                            </Select>
                          </Form.Item>

                          <span>Ngày sinh</span>
                          <Form.Item
                            name="dob"
                            rules={[
                              {
                                required: true,
                                message: 'Ngày sinh không được để trống'
                              }
                            ]}
                          >
                            <DatePicker
                              disabledDate={disabledDate}
                              className="date-picker"
                            />
                          </Form.Item>

                          <span>Số điện thoại</span>
                          <Form.Item
                            name="phone"
                            rules={[
                              {
                                required: true,
                                message: 'Số điện thoại không được để trống'
                              }
                            ]}
                          >
                            <Input addonAfter={<PhoneOutlined />} />
                          </Form.Item>

                          <span>Email</span>
                          <Form.Item
                            name="email"
                            rules={[
                              {
                                required: true,
                                message: 'Email không được để trống'
                              },
                              {
                                type: 'email',
                                message: 'Email không đúng định dạng'
                              }
                            ]}
                          >
                            <Input addonAfter={<MailOutlined />} />
                          </Form.Item>

                          <span>Mật khẩu</span>
                          <Form.Item
                            name="password"
                            rules={[
                              {
                                required: true,
                                message: 'Mật khẩu không được để trống'
                              },
                              {
                                min: 8,
                                message: 'Mật khẩu tối thiểu 8 ký tự'
                              },
                              {
                                max: 32,
                                message: 'Mật khẩu tối đa 32 ký tự'
                              }
                            ]}
                          >
                            <Input.Password type="password" addonAfter={<LockOutlined />} />
                          </Form.Item>

                          <span>Nhập lại mật khẩu</span>
                          <Form.Item
                            name="confirm"
                            dependencies={['password']}
                            rules={[
                              {
                                required: true,
                                message: 'Mật khẩu không được để trống'
                              },
                              {
                                min: 8,
                                message: 'Mật khẩu tối thiểu 8 ký tự'
                              },
                              {
                                max: 32,
                                message: 'Mật khẩu tối đa 32 ký tự'
                              },
                              ({ getFieldValue }) => ({
                                validator(_, value) {
                                  if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve()
                                  }
                                  return Promise.reject(new Error('Mật khẩu nhập lại không khớp!'))
                                }
                              })
                            ]}
                          >
                            <Input.Password type="password" addonAfter={<LockOutlined />} />
                          </Form.Item>

                          {/* <div className='forgot' onClick={()=> router.push('/user/forgot')}><a>Quên mật khẩu</a></div> */}
                          <Form.Item
                            wrapperCol={{
                              offset: 8,
                              span: 16,
                            }}
                          >
                            <Space>
                              <Button
                                // loading={auth.isLoading}
                                danger
                                onClick={()=>{setEditModal(false)}}
                                style={{ marginTop: 20 }}
                              >
                                Thoát
                              </Button>
                              <Button
                                // loading={auth.isLoading}
                                type="primary"
                                htmlType="submit"
                                style={{ marginTop: 20 }}
                              >
                                Sửa
                              </Button>
                            </Space>
                          </Form.Item>
                        </Form>
                      }
                    </div>
                  </Modal>
                </Space>
              } layout="vertical" bordered>

                <Descriptions.Item label="Tài khoản">{auth.profile.name}</Descriptions.Item>
                <Descriptions.Item label={auth.profile.name} span={3}>{auth.profile.email}</Descriptions.Item>
                <Descriptions.Item label="Ngày sinh" span={3}>{(moment(auth.profile.dob).format('DD/MM/YYYY h:mm'))}</Descriptions.Item>
                <Descriptions.Item label="Ngày đăng kí" span={3}>{moment(auth.profile.created_at).format('DD/MM/YYYY')}</Descriptions.Item>
                <Descriptions.Item label="Lần cập nhật thông tin gần nhất" span={3}><Badge status="processing" text={moment(auth.profile.updated_at).format('DD/MM/YYYY')}/></Descriptions.Item>
                <Descriptions.Item label="Loại tài khoản">
                {
                  auth.profile.role === 1 ?
                  <>Quản trị viên</>
                  :
                  <>Tài khoản người dùng thông thường</>
                }
                </Descriptions.Item>
                {/* <Descriptions.Item label="Phòng ban">Lập trình</Descriptions.Item>
                <Descriptions.Item label="Số lượng tài liệu cá nhân">12</Descriptions.Item> */}
              </Descriptions>
              
            </Col>
          </Row>
        </div>
      </div>
    }
    </div>
  )
}
  export default About