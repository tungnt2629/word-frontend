import {
  DEPARTMENTS, SET_LOADING
} from '../actions/types'

const initState = {
  currentDepartment: null,
  listDepartments: [],
  listTemplates: [],
  isLoading: false
}

const documentReducer = (state = initState, action) => {
  switch (action.type) {
  case SET_LOADING:
    return {
      ...state,
      isLoading: action.payload
    }
  case DEPARTMENTS.CREATE_DEPARTMENT:
    return {
      ...state,
      currentDepartment: action.payload
    }
  case DEPARTMENTS.UPDATE_DEPARTMENT:
    return {
      ...state,
      currentDepartment: action.payload
    }
  case DEPARTMENTS.LIST_DEPARTMENT:
    return {
      ...state,
      listDepartments: action.payload
    }
  case DEPARTMENTS.LIST_TEMPLATE:
    return {
      ...state,
      listTemplates: action.payload
    }
  default:
    return state
  }
}

export default documentReducer
