import {
  DOCS, SET_LOADING
} from '../actions/types'

const initState = {
  currentDocument: null,
  listDocuments: [],
  listDocumentsShare: [],
  listDocumentsPublic: [],
  sharePersons: [],
  listComments: [],
  listUserComments: [],
  listCommentAndUserComments: [],
  isLoading: false
}

const documentReducer = (state = initState, action) => {
  
  switch (action.type) {
  case SET_LOADING:
    return {
      ...state,
      isLoading: action.payload
    }
  case DOCS.CREATE_DOCUMENT:
    return {
      ...state,
      currentDocument: action.payload
    }
  case DOCS.UPDATE_DOCUMENT:
    return {
      ...state,
      currentDocument: action.payload
    }
  case DOCS.LIST_DOCUMENT:
    return {
      ...state,
      listDocuments: action.payload
    }
  case DOCS.LIST_DOCUMENT_SHARE:
    return {
      ...state,
      listDocumentsShare: action.payload
    }
  case DOCS.SET_SHARE_PERSON:
    return {
      ...state,
      sharePersons: action.payload
    }
  case DOCS.SET_LIST_COMMENT:
    return {
      ...state,
      listComments: action.payload
    }
  case DOCS.SET_LIST_USER_COMMENT:
    return {
      ...state,
      listUserComments: action.payload
    }
  case DOCS.SET_LIST_COMMENT_AND_USER_COMMENT:
    return {
      ...state,
      listCommentAndUserComments: action.payload
    }
  case DOCS.SET_PUBLIC_DOCUMENT:
    return {
      ...state,
      listDocumentsPublic: action.payload
    }
  default:
    return state
  }
}

export default documentReducer
