import {
    USERS, SET_LOADING
  } from '../actions/types'
  
  const initState = {
    currentUser: null,
    listUsers: [],
    isLoading: false
  }
  
  const userReducer = (state = initState, action) => {
    switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload
      }
    case USERS.CREATE_USER:
      return {
        ...state,
        currentUser: action.payload
      }
    case USERS.UPDATE_USER:
      return {
        ...state,
        currentUser: action.payload
      }
    case USERS.LIST_USER:
      // console.log(action.payload)
      return {
        ...state,
        listUsers: action.payload
      }
    default:
      return state
    }
  }
  
  export default userReducer
  