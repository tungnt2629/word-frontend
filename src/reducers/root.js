import { combineReducers } from 'redux'
import authReducer from './auth'
import documentReducer from './document'
import departmentReducer from './department'
import userReducer from './user'

const rootReducer = combineReducers ({
  auth: authReducer,
  document: documentReducer,
  department: departmentReducer,
  user: userReducer
})

export default rootReducer
