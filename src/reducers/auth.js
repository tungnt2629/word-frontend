import { AUTH, SET_LOADING } from '../actions/types'

const initState = {
  profile: null,
  loading: false,
  error: null
}

const authReducer = (state = initState, action) => {
  switch (action.type) {
  case SET_LOADING:
    return {
      ...state,
      loading: action.payload
    }
  case AUTH.SET_USER:
    return {
      ...state,
      profile: action.payload
    }
  case AUTH.SET_ERROR_MESSAGE:
    return {
      ...state,
      error: action.payload
    }
  default:
    return state
  }
}

export default authReducer
